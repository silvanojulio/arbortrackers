
gcloud config set project arbortrackers-software

gcloud auth activate-service-account --key-file ./arbortrackers-software-id.json

gcloud builds submit --tag gcr.io/arbortrackers-software/arbor-trackers-web
