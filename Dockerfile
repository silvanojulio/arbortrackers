# Compiling client
FROM node:12.18.2-alpine AS base
WORKDIR /app

# ---- Client Dependencies ----
COPY client/package*.json ./

# ---- Now installing client app dependencies ---------
RUN npm install

# ---- Copy Files/Build ----
FROM base AS client-build
WORKDIR /app
COPY client ./

#-Envieronment Variables for UI
ENV REACT_APP_API_URL=https://arbor-trackers-web-epwwv4aipa-uw.a.run.app/api/

# Build front-end static files
RUN npm run build

# --- Release with Alpine ----
FROM node:12.18.2-alpine AS release
WORKDIR /app
COPY server/package*.json ./

RUN npm install

COPY server ./

COPY --from=client-build /app/build ./dist/public

#-Envieronment Variables for UI
ENV REACT_APP_API_URL=https://arbor-trackers-web-epwwv4aipa-uw.a.run.app/api/
ENV APP_SESSION_HOURS=24
ENV APP_JWT_KEY=ARBOR_TRACKERS_2021
ENV MONGO_STRING=mongodb+srv://admin:TcJTiVKo5a4rAwSC@cluster0.c76d0.mongodb.net/myFirstDatabase?retryWrites=true&w=majority
ENV MONGO_DATABASE=ArborTrackersDB
ENV USER_ADMIN_EMAIL=silvanojulio@gmail.com
ENV USER_ADMIN_PASSWORD=Lili2021
ENV TIMEZONE=-1
ENV COMPANY_NAME=Arbor_Trackers
ENV PORT=8080

ENV MAILGUN_DOMAIN=email.arbortrackers.com
ENV MAILGUN_API_KEY=2467c3ea272e2669f78ddabfc3b9e705-71b35d7e-148ce714
ENV EMAIL_FROM=arbortrackers@gmail.com

RUN npm run build

# start app
CMD ["npm", "start"]
