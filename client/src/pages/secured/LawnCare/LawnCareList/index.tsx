import Button from "@material-ui/core/Button";
import React from "react";
import styles from "./index.module.scss";
import { Link } from 'react-router-dom';
import { IconButton, Typography } from "@material-ui/core";
import { DataGrid, GridCellParams, GridColDef, GridColType } from "@material-ui/data-grid";
import { useInit } from "../../../../common/utils/useInit";
import LawnCareProgramService from "../../../../services/lawnCareProgram.service";
import { ILawnCareProgramEstimateView } from "../../../../models/lawnCareProgramTemplate";
import { FaEdit } from "react-icons/fa";
import { RiDeleteBin5Line } from "react-icons/ri";
import moment from 'moment';
import download from 'downloadjs'
import { GrDocumentPdf } from "react-icons/gr";
import { BsDownload } from "react-icons/bs";

interface LawnCareListProps {

}

const _columns: GridColDef[] = [
  
  { field: 'date', headerName: 'Date', width: 110,
    renderCell: (params: GridCellParams) =>  moment(new Date(params.row.date)).format("MM/DD/YYYY") },
  { field: 'customer', headerName: 'Customer', width: 350,
    renderCell: (params: GridCellParams) =>  params.row.customer.fullName },
  { 
    field: 'serviceLocation', headerName: 'Location', width: 350,
    renderCell: (params: GridCellParams) => `${params.row.serviceLocation.address}, ${params.row.serviceLocation.city}, ${params.row.serviceLocation.state}`
  },
  { field: 'status', headerName: 'Status', width: 110 },
  { field: 'type', headerName: 'Type', width: 110, 
    renderCell: (params: GridCellParams) =>  params.row.baseProgramItems?.find((x:any)=>x.code === "TYPE")?.extraOptions},
];

function LawnCareList({  }: LawnCareListProps){

  const estimatesData = useInit(LawnCareProgramService.getAll);

  const onEdit = (estimate: any) => {
    const programEstimateView = estimate as ILawnCareProgramEstimateView;
  };

  const onRemove = (estimate: any) => {
    const programEstimateView = estimate as ILawnCareProgramEstimateView;
    
  };

  const onDownloadPdf = async (estimate: any) => {
    const programEstimateView = estimate as ILawnCareProgramEstimateView;
    const response = await LawnCareProgramService.getEstimatePdf(programEstimateView._id!);
    const content = response.headers['content-type'];
    download(response.data, `estimate_${programEstimateView._id}.pdf`, content);
  };

  const rows = estimatesData.data?.map((x:any) => ({
    ...x,
    id: x._id,
  }));

  const columns = [..._columns,
    {
      field: 'actions', headerName: 'Actions', width: 200 ,
      renderCell: (params: GridCellParams) => (      
        <div>
          <IconButton onClick={()=>{onEdit(params.row)}} color="primary">
            <FaEdit />
          </IconButton>
  
          <IconButton onClick={()=>{onRemove(params.row)}} color="secondary">
            <RiDeleteBin5Line />
          </IconButton>
  
          <IconButton href={`${process.env.REACT_APP_API_URL}html/estimate/${params.row._id}` } 
            target="_blank"
            color="primary">
            <GrDocumentPdf />
          </IconButton>

          <IconButton href={`${process.env.REACT_APP_API_URL}pdf-redirect/estimate/${params.row._id}` } 
            target="_blank"
            color="primary">
            <BsDownload />
          </IconButton>
          
        </div>
      )
    }]

  return (
    <div className={styles.root}>       

       <Typography className={styles.title} variant="h5" component="h5">          
          Lawn Care Programs Estimates
        </Typography>
        
        <Button variant="outlined" color="primary" href="/#!/lawn-care/new" className={styles.addCustomer}>
          Add Estimate
        </Button>
        
        <div style={{ height: 400, width: '100%' }}>
          <DataGrid rows={rows || []} columns={columns} pageSize={5} loading={estimatesData.loading} />
        </div>
    
    </div>);
};

export default LawnCareList;
