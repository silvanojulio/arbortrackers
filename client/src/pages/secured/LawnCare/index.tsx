import {
  Switch,
  Route,
  BrowserRouter as Router,
} from "react-router-dom";
import LawnCareEdit from "./LawnCareEdit";
import LawnCareList from "./LawnCareList";
interface LawnCareProps {

}

function LawnCare({  }: LawnCareProps){
  return (
    <Switch>
      <Route component={LawnCareEdit} path="/lawn-care/edit/:id" />
      <Route component={LawnCareEdit} path="/lawn-care/new" />
      <Route exact component={LawnCareList} path="/lawn-care" />
    </Switch>
  );
};

export default LawnCare;
