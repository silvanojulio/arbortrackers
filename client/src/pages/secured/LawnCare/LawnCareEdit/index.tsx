import { useHistory } from "react-router-dom";
import { useInit } from "../../../../common/utils/useInit";
import LawnCareProgramForm from "../../../../components/system/Forms/LawnCareProgramForm";
import LawnCareProgramService from "../../../../services/lawnCareProgram.service";
import styles from "./index.module.scss";

interface LawnCareEditProps {

}

function LawnCareEdit({  }: LawnCareEditProps){

  const template = useInit(LawnCareProgramService.getTemplate);
  const history = useHistory();
  
  const onCancel = () => {
    history.push('/lawn-care')
  };

  const onConfirm = () => {
    history.push('/lawn-care')
  };

  return (
    <div className={styles.root}>

      <div>
        {template.data && <LawnCareProgramForm 
          template={template.data}
          onConfirm={onConfirm}
          onCancel={onCancel}
          />}
      </div>

    </div>);
};

export default LawnCareEdit;
