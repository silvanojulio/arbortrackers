import  React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import styles from "./index.module.scss";
import EditCustomerForm from "../../../components/system/Forms/EditCustomerForm";
import { DataGrid, GridColDef, GridValueGetterParams, GridCellParams } from '@material-ui/data-grid';
import { ICustomer } from "../../../models/customer";
import CustomerService from "../../../services/customer.service";
import Typography from "@material-ui/core/Typography";
import {FaEdit} from "react-icons/fa";
import { ImHome } from "react-icons/im";
import {RiDeleteBin5Line} from "react-icons/ri";
import IconButton from '@material-ui/core/IconButton';
import ConfirmationDialog from "../../../components/library/ConfirmationDialog";
import Progress from "../../../components/library/Progress";
import { useBlockScreen } from "../../../components/library/BlockScreen";
import CustomerServiceLocationsList from "../../../components/system/CustomerServiceLocationsList";

interface CustomersProps {

}

const _columns: GridColDef[] = [
  
  { field: 'fullName', headerName: 'Full Name', width: 220 },
  { field: 'email', headerName: 'Email', width: 220 },
  { field: 'phone', headerName: 'Phone', width: 220 },
];

function Customers({  }: CustomersProps){

  const [showEditCustomer, setShowEditCustomer] = useState(false);
  const [showCustomerServiceLocationsList, setShowCustomerServiceLocationsList] = useState(false);
  const [loading, setLoading] = useState(false);
  const [customers, setCustomers] = useState<ICustomer[]>([]);
  const [selectedCustomer, setSelectedCustomer] = useState<ICustomer|undefined>();
  const [confirmationOpen, setConfirmationOpen] = useState(false);

  const blockScreen = useBlockScreen();
  
  const init = async () => {
    try {
      setLoading(true);
      setCustomers(await CustomerService.getAll());
    } catch (error: any) {
      
    }finally{
      setLoading(false);
    }
  };

  useEffect(()=> {

    init();

  }, []);

  const onNewCustomer = () => {
    setShowEditCustomer(true);
    setSelectedCustomer(undefined);
  }

  const onEditCustomer = (customer: any) => {    
    setSelectedCustomer(customer as ICustomer);
    setShowEditCustomer(true);
  }

  const onRemoveCustomer = (customer: any) => {    
    setSelectedCustomer(customer as ICustomer);
    setConfirmationOpen(true);
  }

  const onServiceLocationList = (customer: any) => {
    setSelectedCustomer(customer as ICustomer);
    setShowCustomerServiceLocationsList(true);
  }

  const onCustomerDeleteConfirmed = async () => {

    try {      
      blockScreen(true, "Please wait while the item is being deleted...")
      setConfirmationOpen(false);
      if(selectedCustomer?._id){
        await CustomerService.deleteCustomer(selectedCustomer._id);
        await init(); 
      }
    } catch (error: any) {
      
    }finally{
      blockScreen(false);
    }
  }

  const onEditCustomerClose = (createdCustomer?: ICustomer) => {
    setShowEditCustomer(false);
    init();
    if(createdCustomer?._id && !selectedCustomer){
      setSelectedCustomer(createdCustomer);
      setShowCustomerServiceLocationsList(true);
    }
  }

  const rows = customers.map(x => ({
    ...x,
    id: x._id,
  }));

  const columns = [..._columns,
  {
    field: 'isActive', headerName: 'Active', width: 130 ,
    renderCell: (params: GridCellParams) => (<div>
      {params.row.active? "Yes" : "No"}
    </div>)
  },
  {
    field: 'active', headerName: 'Actions', width: 200 ,
    renderCell: (params: GridCellParams) => (      
      <div>
        <IconButton onClick={()=>{onEditCustomer(params.row)}} color="primary">
          <FaEdit />
        </IconButton>

        <IconButton onClick={()=>{onRemoveCustomer(params.row)}} color="secondary">
          <RiDeleteBin5Line />
        </IconButton>

        <IconButton onClick={()=>{onServiceLocationList(params.row)}} color="default">
          <ImHome />
        </IconButton>
      </div>
    )
  }]

  return (
    <div className={styles.root}>
      <Typography className={styles.title} variant="h5" component="h5">          
        Customers
      </Typography>
      
      <Button variant="outlined" color="primary"
        onClick={onNewCustomer} className={styles.addCustomer}>
        Add Customer
      </Button>
      
      <div style={{ height: 400, width: '100%' }}>
        <DataGrid rows={rows} columns={columns} pageSize={5} loading={loading} />
      </div>

      {showEditCustomer && <EditCustomerForm 
        key={`edit-customer-form-${showEditCustomer}`}
        show={showEditCustomer} 
        customerToEdit={selectedCustomer}
        onClose={()=>setShowEditCustomer(false)} 
        onCreated={onEditCustomerClose}/>}

      {selectedCustomer && <CustomerServiceLocationsList 
        key={`service-locations-${selectedCustomer?._id}`}
        customer={selectedCustomer} 
        show={showCustomerServiceLocationsList}
        onClose={()=>setShowCustomerServiceLocationsList(false)} />}

      <ConfirmationDialog title="Delete Customer" open={confirmationOpen} 
        onCancel={()=>setConfirmationOpen(false)}
        onAccept={onCustomerDeleteConfirmed}>
        Are you sure you want to delete the selected customer?
      </ConfirmationDialog>
    </div>);
};

export default Customers;
