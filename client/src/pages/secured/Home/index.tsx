import { useState } from "react";

interface HomeProps {

}

function Home({  }: HomeProps){

  console.log("Pasó por Home");

  const [total, setTotal] = useState<number>(0);  

  const onSumarUno = () => {
    console.log("Pasó por onSumarUno");
    setTotal(total + 1);
  }

  return (
    <div>
     Home
    </div>
  );
};

export default Home;
