import React, { useEffect } from "react";
import Elevation from "../../../components/library/Elevation";
import TextInput from "../../../components/library/TextInput";
import Text from "../../../components/library/Text";
import styles from "./index.module.scss";
import { Button } from "../../../components/library/Button";
import AuthService from "../../../services/auth.service";
import { useForm } from "react-hook-form";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useToasts } from 'react-toast-notifications'
import AppStorage from "../../../common/utils/appStorage";
import { useHistory } from "react-router-dom";
import { useBlockScreen } from "../../../components/library/BlockScreen";

const loginFormSchema = Yup.object().shape({
  email: Yup.string().email().required("Required"),
  password: Yup.string().required("Required"),
});

function Login(){
  
  let history = useHistory();
  const blockScreen = useBlockScreen();

  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(loginFormSchema),
  });

  const { addToast } = useToasts()

  useEffect(()=>{
    AppStorage.removeValue("app-session");
    AppStorage.removeValue("app-security-token");
  }, [])

  const onLogin = async (data: { email: string, password: string }) => {
    
    try {
      blockScreen(true, "Validating credentials...");
      const session = await AuthService.login(data.email, data.password);
      
      AppStorage.setValue("app-session", session.appUser);
      AppStorage.setValue("app-security-token", session.securityToken);

      addToast("Welcome " + session.appUser.fullName, {
        appearance: 'info',
        autoDismiss: true,
      });

      history.push("/home");

    } catch (error: any) {
      addToast("Login error: " + error.response.data.message, {
        appearance: 'error',
        autoDismiss: true,
      })
      console.error("Login error:", error.response);
    }finally{
      blockScreen(false);
    }
  }

  return (
    <div className={styles.root}>
      <div className={styles.authContent}>
      
      <Elevation elevation={4} className={styles.card}>

        <Text.Title className={styles.title}>Arbor Trackers Login</Text.Title>

        <form className={styles.form} onSubmit={handleSubmit(onLogin)}> 
          
          <TextInput 
            name="email"
            ref={register}
            field={{
                label: "Email",
                error:errors?.email,                
              }}
            />

          <TextInput
            name="password"
            type="password"
            ref={register}
            field={{
              label: "Password",
              className: styles.field,
              error: errors?.password,
            }}/>
            
          <Button variant="primary"
            type="submit"
            classN={styles.enterButton}>Enter</Button>
        </form>
      </Elevation>      

      </div>
    </div>);
};

export default Login;
