import './App.css';
import { ToastProvider} from 'react-toast-notifications';

import {
  HashRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import Login from './pages/public/Login';
import Home from './pages/secured/Home';
import LawnCare from './pages/secured/LawnCare';
import PlatHealthCare from './pages/secured/PlatHealthCare';
import Customers from './pages/secured/Customers';
import PrivateLayout from './components/system/PrivateLayout';
import BlockScreen, { BlockScreenContext } from './components/library/BlockScreen';
import Users from './pages/secured/Users';

function App() {

  return (
    <ToastProvider>
      <BlockScreenContext.Provider value={{}} >
        <BlockScreen >
          <Router hashType="hashbang">
            <Switch>          
              <Route component={Login} path="/login" />          
              <Route path="/">            
                <PrivateLayout>
                  <Switch>
                    <Route component={Home} path="/home" />
                    <Route component={Customers} path="/customers" />
                    <Route component={LawnCare} path="/lawn-care" />
                    <Route component={PlatHealthCare} path="/plant-health-care" />
                    <Route component={Users} path="/users" />
                    <Redirect path="/" to="/home"/>
                  </Switch>
                </PrivateLayout>
              </Route>          
            </Switch>      
          </Router>
        </BlockScreen>
      </BlockScreenContext.Provider>
    </ToastProvider>
  );
    
}

export default App;
