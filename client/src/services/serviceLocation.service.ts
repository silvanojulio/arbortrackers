import { IServiceLocation } from '../models/serviceLocation';
import api from './api';

const apiPath = "/service-location";
async function create(serviceLocation: IServiceLocation): Promise<IServiceLocation> {
  const response = await api.post<IServiceLocation>(apiPath, serviceLocation);
  return response.data;
}

async function deleteServiceLocation(serviceLocationId: string) {
  await api.delete(`${apiPath}/${serviceLocationId}`);
}

async function update(serviceLocation: IServiceLocation) {
  await api.put<IServiceLocation>(`${apiPath}/${serviceLocation._id}`, serviceLocation);
}

async function getByCustomerId(customerId: string): Promise<IServiceLocation[]> {
  const response = await api.get<IServiceLocation[]>(`${apiPath}/customer/${customerId}`);
  return response.data;
}

const ServiceLocationService = {
  create,
  deleteServiceLocation,
  update,
  getByCustomerId,
}

export default ServiceLocationService;
