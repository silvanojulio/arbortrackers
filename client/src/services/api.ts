import axios from "axios";
import AppStore from "./../common/utils/appStorage";
import * as dotenv from "dotenv";
dotenv.config();

const api = axios.create({
  baseURL: process.env.REACT_APP_API_URL || "https://arbor-trackers-web-epwwv4aipa-uw.a.run.app/api/",
});

api.interceptors.request.use(
  function (config) {

    config.headers = {
      ...config.headers,
      'security-token': AppStore.getString('app-security-token'),
      "Content-Type": "application/json",
    };
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

api.interceptors.response.use(function (response) {
  // @TODO: error notifications (Can be toast or alert as example)
  if (response.status === 401) {
    //Navigate to login
  }else if (response.status === 403) {
    //Permission error
  }
  return response;
});

export default api;
