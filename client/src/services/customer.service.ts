import { ICustomer } from '../models/customer';
import api from './api';

async function create(cliente: ICustomer): Promise<ICustomer> {
  const response = await api.post<ICustomer>(`/customer`, cliente);
  return response.data;
}

async function deleteCustomer(clienteId: string) {
  await api.delete<ICustomer>(`/customer/${clienteId}`);
}

async function updateCustomer(cliente: ICustomer) {
  await api.put<ICustomer>(`/customer/${cliente._id}`, cliente);
}

async function getAll(): Promise<ICustomer[]> {
  const response = await api.get<ICustomer[]>(`/customer`);
  return response.data;
}

const CustomerService = {
  create,
  updateCustomer,
  deleteCustomer,
  getAll,
}

export default CustomerService;
