import IAppSession from '../models/appSession';
import api from './api';

async function login(email: string, password: string): Promise<IAppSession> {
  const response = await api.post<IAppSession>(`/session/login`, {
    email,
    password
  });

  const session = response.data;

  return session;
}

async function getCurrentSession(): Promise<IAppSession> {
  const response = await api.get<IAppSession>(`/current-session`);
  const session = response.data;
  return session;
}


const AuthService = {
    login,
    getCurrentSession,
}

export default AuthService;
