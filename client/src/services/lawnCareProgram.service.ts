import { ILawnCareProgramEstimateView, ILawnCareProgramTemplate } from '../models/lawnCareProgramTemplate';
import api from './api';

const apiPath = "/lawn-care-program/estimate";

async function getTemplate(): Promise<ILawnCareProgramTemplate> {
  const response = await api.get<ILawnCareProgramTemplate>(`${apiPath}/template`);
  return response.data;
}

async function getAll(){
  const response = await api.get(`${apiPath}`);
  return response.data as ILawnCareProgramEstimateView[];
}

async function createEstimate(date: Date, customerId: string, serviceLocationId: string,
  baseProgramItems: any, supplementalServiceItems: any, sendEmail: boolean){
  
    const baseItems = Object.keys(baseProgramItems)
      .map(key=>({...baseProgramItems[key], code: key}))
      .filter(item=>item.checked);
    const supplementalItems = Object.keys(supplementalServiceItems)
      .map(key=>({...supplementalServiceItems[key], code: key}))
      .filter(item=>item.checked);

    const data = {
      date,
      customerId,
      serviceLocationId,
      baseProgramItems: baseItems,
      supplementalServiceItems: supplementalItems,
      sendEmail
    };

    await api.post<ILawnCareProgramTemplate>(apiPath, data);
}

async function getEstimatePdf(estimateId: string){
  const response = await api.get(`${apiPath}/pdf/${estimateId}`,{
    responseType: 'blob',
  });
  return response;
}

const LawnCareProgramService = {
  getTemplate,
  createEstimate,
  getAll,
  getEstimatePdf,
}

export default LawnCareProgramService;
