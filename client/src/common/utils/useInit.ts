import { useEffect, useState } from "react";

export interface IUseInitResult<T> {
  data?: T;
  loading: boolean;
  error?: any;
}

export function useInitWitKey<T>(
  getResult: () => Promise<T>,
  defaultValue?: T,
  key?: any
): IUseInitResult<T> {
  const [result, setResult] = useState<T | undefined>(defaultValue);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<any>();

  useEffect(
    () => {
      const init = async () => {
        try {
          setLoading(true);
          const r = await getResult();
          setResult(r);
        } catch (error: any) {
          setError(error);
        } finally {
          setLoading(false);
        }
      };

      init();
    },
    key ? [key] : []
  );

  return {
    data: result,
    loading,
    error,
  };
}

export function useInit<T>(
  getResult: () => Promise<T>,
  defaultValue?: T
): IUseInitResult<T> {
  return useInitWitKey(getResult, defaultValue, null);
}
