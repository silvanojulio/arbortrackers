export default class AppStorage {
    
    static async setValue(key:string, value: any) {
        localStorage.setItem(key, typeof value === 'object'? JSON.stringify(value) : value);
    }

    static getObject<T>(key:string):T{
        return JSON.parse(localStorage[key]) as T;
    }

    static getString(key:string):string{
        return localStorage[key];
    }

    static removeValue(key:string){
        return localStorage.removeItem(key);
    }
}
