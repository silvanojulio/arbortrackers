import { useEffect, useState } from "react";

export interface FetchError {
  code: number;
  message: string;
  error?: any;
}

export interface IUseFetch<T> {
  data?: T;
  error?: FetchError;
  pending: boolean;
}

export const useFetch = <T>(
  key: any,
  fetchAction: () => Promise<T>
): IUseFetch<T> => {
  const [result, setResult] = useState<IUseFetch<T>>({
    pending: true,
  });

  useEffect(() => {
    const f = async () => {
      try {
        setResult({ pending: true });
        const data = await fetchAction();
        setResult({
          pending: false,
          data,
        });
      } catch (error: any) {
        setResult({
          pending: false,
          error: {
            code: 0,
            message: "Server error",
            error,
          },
        });
      }
    };

    f();
  }, [fetchAction, key]);

  return result;
};

export function useFetchWithParams<T>(
  fetchAction: (...args: any[]) => Promise<T>,
  ...params: any[]
): IUseFetch<T> {
  const key = JSON.stringify(params);
  const result = useFetch(key, async () => await fetchAction(...params));
  return result;
}
