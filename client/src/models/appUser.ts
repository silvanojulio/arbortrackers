import { IModelBase } from './modelBase';
import { IEnumEntityBase } from "./enumEntityBase";

export interface IAppUser extends IModelBase {
    fullName: string;
    email: string;
    passwordHash: string;
    active: Boolean;
    role: IRoles | null;
}

export interface IRoles extends IEnumEntityBase{}
