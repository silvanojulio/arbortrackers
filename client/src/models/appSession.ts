import { IAppUser } from "./appUser"

export default interface IAppSession {
    appUser: IAppUser; 
    expiration: Date;
    securityToken: string;    
}
