export interface ICustomer {
    _id?: string;
    fullName: string;
    phone?: string;
    email?: string;
    active: boolean;
}
