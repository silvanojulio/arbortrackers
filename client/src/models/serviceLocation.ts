export interface IServiceLocation {
    _id?: string;
    address:string;
    cp:string;
    state:string;
    city:string;
    customer_id: string;
    phone?:string;
    email?:string;
    contact?:string;
    point?:number[];
    isMain?:boolean;
}
