export class IModelBase{
    _id: string="";
    public isNew(): boolean{
        return this._id===null || this._id ===undefined || this._id === "";
    }
}
