import { ICustomer } from "./customer";
import { IServiceLocation } from "./serviceLocation";

export interface IProgramItem{
    code: string;
    title: string;
    subtitle?: string;
    description?: string;
    explanation?: string;
    applications?: boolean;
    extraOptions?: string[];
    showCost?: boolean;
}

export interface ILawnCareProgramTemplate{
    title: string;
    logoUrl?: string;
    descripcion: string;
    bottomText: string;
    types: string[],
    author: {
        name: string;
        phone: string;
        email: string; 
    },
    baseProgram: {
        title: string;
        explanation: string;
        items: IProgramItem[]
    },
    suplementalProgram: {
        items: IProgramItem[]
    }
}

export interface IBaseProgramEstimateItem{
    code: string;
    checked: boolean;
    label: string;
    cost: number;
    applications?: number; 
    extraOptions?: string[]; 
}

export interface ILawnCareProgramEstimate {
    _id?: string;
    date: Date,
    status: string,
    serviceLocation_id?: string,
    customer_id?: string,
    baseProgramItems: IBaseProgramEstimateItem[],
    supplementalServiceItems: IBaseProgramEstimateItem[],  
}

export interface ILawnCareProgramEstimateView{
    _id?: string;
    date: Date,
    status: string,
    serviceLocation_id: string,
    customer_id: string,
    baseProgramItems: IBaseProgramEstimateItem[],
    supplementalServiceItems: IBaseProgramEstimateItem[],
    userCreateId?: string,
    customer: ICustomer,
    serviceLocation: IServiceLocation,
}
