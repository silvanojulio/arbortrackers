export interface IEnumEntityBase {
    id: number;
    description: string;
}
