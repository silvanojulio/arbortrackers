import React from "react";
import styles from "./index.module.scss";

interface LoadingPageProps {
  message?: string;
}

function LoadingPage({ message }: LoadingPageProps){
  return (
    <div className={styles.root}>
    this is LoadingPage react component
    <h1>{message}</h1>
    </div>);
};

export default LoadingPage;
