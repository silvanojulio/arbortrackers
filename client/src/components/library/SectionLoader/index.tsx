import React from "react";
import Progress from "../Progress";
import styles from "./index.module.scss";

interface SectionLoaderProps {
  show: boolean;
  message?: string;
}

function SectionLoader({ message, show }: SectionLoaderProps){
  
  if(!show) return null;
  
  return (
    <div className={styles.root}>
    <Progress.RimeCube className={styles.loader} message={message}></Progress.RimeCube>
    </div>);
};

export default SectionLoader;
