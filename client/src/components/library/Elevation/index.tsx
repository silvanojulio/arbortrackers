import styles from "./index.module.scss";
import cn from "classnames";
import { ReactElement } from "react";

interface ElevationProps {
  children: ReactElement | JSX.Element[];
  className?: string;
  elevation?: 1 | 2 | 3 | 4; 
}

function Elevation({ children, className, elevation = 1 }: ElevationProps){
  return (
    <div className={cn(styles.root, styles['elevation'+elevation], className)}>
      {children}
    </div>);
};

export default Elevation;
