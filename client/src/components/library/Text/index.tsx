import React, { ReactElement } from "react";
import styles from "./index.module.scss";
import cl from "classnames";

interface TextProps {
  children: ReactElement | JSX.Element[] | string | number;
  className?: string;
  heavy?: boolean;
}

interface TextPropsInternal extends TextProps{
  type: string;
}

function TextBase({ children, className, heavy, type }: TextPropsInternal){
  return (
    <div className={cl(styles.root, styles[type], {
      [styles.heavy]: heavy
    }, className)}>
    { children }
    </div>);
};

function Display(props: TextProps){
  return (<TextBase type={"display"} {...props}/>);
};

function Headline(props: TextProps){
  return (<TextBase type={"headline"} {...props}/>);
};

function Title(props: TextProps){
  return (<TextBase type={"title"} {...props}/>);
};

function Subheader(props: TextProps){
  return (<TextBase type={"hubheader"} {...props}/>);
};

function Body(props: TextProps){
  return (<TextBase type={"body"} {...props}/>);
};

function Caption(props: TextProps){
  return (<TextBase type={"caption"} {...props}/>);
};

function Small(props: TextProps){
  return (<TextBase type={"small"} {...props}/>);
};


const Text = {
  Display,
  Headline,
  Title,
  Subheader,
  Body,
  Caption,
  Small,
}

export default Text;
