import styles from "./index.module.scss";
import cn from "classnames";
import React from "react";
import Field, { FieldProps } from "../Field";

interface TextInputProps extends React.InputHTMLAttributes<HTMLInputElement>{
  field: FieldProps;
  classN?: string;
}

const TextInput = React.forwardRef((
  {field, classN, ...props}: TextInputProps, 
  ref: React.HTMLProps<HTMLInputElement>["ref"])=>{
  return (
    <Field {...field}>
       <input className={cn(styles.root, classN)} ref={ref} {...props}/>
    </Field>    
    );
})

export default TextInput;
