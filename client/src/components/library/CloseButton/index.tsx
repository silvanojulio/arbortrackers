import IconButton from "@material-ui/core/IconButton";
import { GrClose } from "react-icons/gr";
import styles from "./index.module.scss";

interface CloseButtonProps {
  onClick?: () => void;
  onClose?: (flag: boolean) => void;
}

function CloseButton({ onClick, onClose }: CloseButtonProps){

  const onCloseClick = () => {
    if(onClick) onClick();
    else if (onClose) onClose(false);
  } 

  return (
    <div className={styles.root}>
     <IconButton onClick={onCloseClick} color="default">
        <GrClose />
      </IconButton>
    </div>);
};

export default CloseButton;
