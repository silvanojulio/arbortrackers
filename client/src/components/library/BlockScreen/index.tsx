import classnames from "classnames";
import React, { useContext, useState } from "react";
import Progress from "../Progress";
import styles from "./index.module.scss";

interface BlockScreenProps {
  children: any;
}

function BlockScreen({children }: BlockScreenProps){

  const [block, setBlock] = useState<{
    block: boolean;
    message?: string;
  }|null>(null);

  const contex = useContext(BlockScreenContext);
  
  contex.setBlock = (block: boolean, message?: string) => {
    setBlock({
      block,
      message
    });
  }
  
  const isBlocked = block?.block;

  return (
    <>
      {isBlocked && 
      <div className={styles.root}>
        <Progress.RimeCube className={styles.loader} message={block?.message}></Progress.RimeCube>
      </div>}
      <div className={classnames({
        [styles.blockContent]: isBlocked
      })}>
        {children}
      </div>
      
    </>
  );
};

export const useBlockScreen = () => {
  const contex = useContext(BlockScreenContext);
  return contex.setBlock ||((block: boolean, message?: string) => {});
}

export interface IBlockScreenContext{
  message?: string;
  setBlock?: (block: boolean, message?: string)=>void;
}

export const BlockScreenContext = React.createContext<IBlockScreenContext>({});

export default BlockScreen;
