import cn from "classnames";
import { ReactElement } from "react";
import styles from "./index.module.scss";

export interface FieldProps {
  label?: string;
  error?: {
    message?: string;
  };
  className?: string;
}

interface FieldPropsInternal extends FieldProps{
  children: ReactElement | JSX.Element[]; 
}

function Field(props: FieldPropsInternal){
  return (
    <div className={cn(styles.root, props.className)}>
    {!!props.label && <div className={styles.label}>{props.label}</div>}

    {props.children}

    {!!props.error?.message && <div className={styles.error}>{props.error?.message}</div>}
  </div>);
};

export default Field;
