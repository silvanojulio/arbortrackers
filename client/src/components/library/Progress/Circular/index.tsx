import classnames from "classnames";
import styles from "./index.module.scss";

const PI = 3.14;
const PENDING_PROGRESS_SIZE = 35; // used for pending animation

const Variant = {
  dark: styles.dark,
  light: styles.light,
};

export interface ICircularProgress {
  className?: string;
  progress?: number;
  size?: number;
  variant?: "light" | "dark";
}

function getValueOfPercentage(percentage: number, totalValue: number) {
  return Math.ceil((percentage * totalValue) / 100);
}

function CircularProgress({
  className,
  progress,
  size: diameter = 40,
  variant = "dark",
}: ICircularProgress) {
  const strokeWidth = Math.ceil(diameter * 0.08); // Stroke width 8% of the circle diameter
  const circumference = Math.ceil(PI * (diameter - strokeWidth * 2));
  const radius = diameter / 2;

  // If progress is negative or 0% or more than 100% then the animation will be played
  // Otherwise, the circle will display the progress fill in %
  const isAnimated = !(Number(progress) > 0 && Number(progress) <= 100);

  const sizeInPx = `${diameter}px`;

  const percentage = isAnimated
    ? getValueOfPercentage(PENDING_PROGRESS_SIZE, circumference) - circumference
    : Math.abs(
        circumference -
          getValueOfPercentage(progress || 100, circumference - strokeWidth)
      );

  return (
    <div
      className={classnames(
        styles.root,
        Variant[variant],
        {
          [styles.animated]: isAnimated,
        },
        className
      )}
    >
      <svg
        height={sizeInPx}
        width={sizeInPx}
        viewBox={`0 0 ${diameter} ${diameter}`}
      >
        <circle
          className={styles.background}
          cx={radius}
          cy={radius}
          r={`${radius - strokeWidth}`}
          strokeWidth={strokeWidth}
        />
        <circle
          className={styles.progressLine}
          strokeDasharray={circumference}
          strokeDashoffset={percentage}
          cx={radius}
          cy={radius}
          r={`${radius - strokeWidth}`}
          strokeWidth={strokeWidth}
        />
      </svg>
    </div>
  );
}

export default CircularProgress;
