import React from "react";
import classNames from "classnames";
import style from "./index.module.scss";

interface RimeCubeProps extends React.HTMLAttributes<HTMLDivElement> {
  className?: string;
  [rest: string]: any;
  message?: string;
}

function RimeCube(props: RimeCubeProps) {
  const { className, rest } = props;
  return (
    <div className={classNames(style.root, className)} {...rest}>
      <div className={style.circle}>
        <div className={style.cube}>
          <div className={classNames(style.side, style.front)} />
          <div className={classNames(style.side, style.back)} />
          <div className={classNames(style.side, style.top)} />
          <div className={classNames(style.side, style.bottom)} />
          <div className={classNames(style.side, style.left)} />
          <div className={classNames(style.side, style.right)} />
        </div>
      </div>
      <h4 className={style.message}>{props.message}</h4>
    </div>
  );
}

export default RimeCube;
