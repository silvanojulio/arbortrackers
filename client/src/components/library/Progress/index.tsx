import CircularProgress, { ICircularProgress } from "./Circular";
import RimeCube from "./RimeCube";

const CircleDark = (args?: ICircularProgress) => {
  const props: ICircularProgress = {
    ...args,
    variant: "dark",
  };
  return <CircularProgress {...props} />;
};

const CircleLight = (args?: ICircularProgress) => {
  const props: ICircularProgress = {
    ...args,
    variant: "light",
  };
  return <CircularProgress {...props} />;
};

const Progress = {
  CircleDark,
  CircleLight,
  RimeCube,
};

export default Progress;
