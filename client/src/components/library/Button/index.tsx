import React from "react";
import cn from "classnames";
import styles from "./index.module.scss";

interface ButtonBaseProps{
  button: React.ButtonHTMLAttributes<HTMLButtonElement>,
  _type: "default" | "outline" | "square";
  variant?: "primary" | "secondary" | "danger" | "warning" | "grey" | "success";
  classN?: string;
}

interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement>{
  variant?: ButtonBaseProps["variant"];
  classN?: string;
}

function ButtonBase({classN, variant="primary", ...props}: ButtonBaseProps){
  return (
    <button className={cn(styles.root, styles[variant], styles[props._type], classN)} {...props.button}>
      {props.button.children}
    </button>);
};

export function Button({classN, ...props}: ButtonProps){
  return (
    <ButtonBase _type={'default'} variant={props.variant} classN={classN} button={props}/>
  )
}

export function OutlineButton({classN, ...props}: ButtonProps){
  return (
    <ButtonBase _type={'outline'} variant={props.variant} classN={classN} button={props}/>
  )
}

export function SquareButton({classN, ...props}: ButtonProps){
  return (
    <ButtonBase _type={'square'} variant={props.variant} classN={classN} button={props}/>
  )
}
