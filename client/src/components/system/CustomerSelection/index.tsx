import { Drawer, Grid, IconButton, TextField, Typography } from "@material-ui/core";
import React, { useMemo, useState } from "react";
import { FaChevronRight } from "react-icons/fa";
import { useInit } from "../../../common/utils/useInit";
import { ICustomer } from "../../../models/customer";
import CustomerService from "../../../services/customer.service";
import CloseButton from "../../library/CloseButton";
import SectionLoader from "../../library/SectionLoader";
import styles from "./index.module.scss";

interface CustomerSelectionProps {
  show: boolean;
  selectServiceLocation?: boolean;
  onClose: ()=>void;
  onSelected: (customer: ICustomer) => void;
}

function Comp(props: CustomerSelectionProps){
  const [text, setText] = useState<string|undefined>();
  const [selectedCustomer, setSelectedCustomer] = useState<ICustomer|undefined>();
  const customers = useInit<ICustomer[]>(CustomerService.getAll, []);
  const filteredCustomers = useMemo(() => {
    if(!(customers?.data)) return [];
    if(!text || text.trim() === "") return customers.data;

    const value = text.toUpperCase();
    return customers.data.filter(x=> 
      x.fullName.toUpperCase().includes(value) ||
      x.email?.toUpperCase().includes(value) || 
      x.phone?.toUpperCase().includes(value) )
  }, [text, customers?.data]);
    
  const onSelectCustomer = (customer:ICustomer) => {
    props.onSelected(customer);
  }
  return (
    <Drawer
      anchor={'left'}
      open={props.show} 
      variant="persistent"
      onClose={props.onClose}>
      
      <div className={styles.root} >

        <CloseButton onClose={props.onClose}/>

        <Typography className={styles.title} variant="h5" component="h5">         
          Select a customer
        </Typography>

        <div>
        
        <TextField className={styles.search}             
            label="Search here" 
            variant="outlined"
            value={text}
            onChange={(e)=>{
              setText(e.target.value);              
            }}/>

        </div>        

        <div className={styles.list}>
          {!customers.loading && 
            filteredCustomers.map(customer => (
              <div key={`id-${customer._id}`}  className={styles.customer}>                
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <strong>{customer.fullName}</strong>
                  </Grid>
                  <Grid item xs={6}>
                    Email: <strong>{customer.email}</strong>
                  </Grid>
                  <Grid item xs={6}>
                    Phone 22: <strong>{customer.phone}</strong>
                  </Grid>
                </Grid>

                <div className={styles.selectIcon}>
                  <IconButton onClick={()=>{onSelectCustomer(customer)}} color="primary">
                    <FaChevronRight />
                  </IconButton>
                </div>
              </div>
            ))
          }

          <SectionLoader show={customers.loading} message="Searching customes"/>

          { !customers.loading && customers.data?.length === 0 &&
            <h3>
              -- No customers found --
            </h3>
          }
        </div>


      </div>

    </Drawer>
    );
};

const CustomerSelection = ({show, ...props}: CustomerSelectionProps) => {
  return <Comp key={`selection-${show}`} show={show} {...props}/> 
}

export default CustomerSelection;
