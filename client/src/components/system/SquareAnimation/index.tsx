import styles from "./index.module.scss";
import cn from "classnames";

interface SquareAnimationProps {

}

function SquareAnimation({  }: SquareAnimationProps){
  return (
    <div className={styles.root}>
      <span className={styles.r}></span>
      <span className={cn(styles.r, styles.s)}></span>
      <span className={cn(styles.r, styles.s)}></span>
      <span className={styles.r}></span>
    </div>);
};

export default SquareAnimation;
