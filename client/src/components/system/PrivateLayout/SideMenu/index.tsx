import { useState } from "react";
import styles from "./index.module.scss";
import { GrGroup } from "react-icons/gr";
import {RiPlantLine} from "react-icons/ri";
import {GiTreeBranch} from "react-icons/gi";
import {VscAccount} from "react-icons/vsc";
import {AiOutlineHome} from "react-icons/ai";
import { useHistory } from "react-router";
import classNames from "classnames";

interface SideMenuProps {
  onMenuSelected: () => void;
}

interface IMenuItem {
  label: string;
  description?: string;
  icon: any;
  isActive?: boolean;
  goTo: string;
}

const appMenu: IMenuItem[] = [
  {label: 'Home', icon: <AiOutlineHome/>, goTo: '/home' },
  {label: 'Customers', icon: <GrGroup/>, goTo: '/customers' },
  {label: 'Lawn Care', icon: <RiPlantLine/>, goTo: '/lawn-care'},
  {label: 'Plant Health Care', icon: <GiTreeBranch/>, goTo: '/plant-health-care'},
  {label: 'Users', icon: <VscAccount/>, goTo: '/users'},
]

function SideMenu({ onMenuSelected }: SideMenuProps){
  const history = useHistory();
  const [currentPath, setCurrentPath] = useState<string>(history.location.pathname);

  return (
    <div className={styles.root}>
      {appMenu.map(item => (
        <div className={classNames(styles.itemMenu, {
          [styles.isSelected]: currentPath === item.goTo
        })}
          onClick={()=>{
            setCurrentPath(item.goTo);
            history.push(item.goTo);
            onMenuSelected();
          }}>
          <span>{item.icon}</span>
          <span>{item.label}</span>
        </div>
      ))}
    </div>);
};

export default SideMenu;
