import { useEffect, useState } from "react";
import {useFetch} from "../../../common/hooks/useFetch";
import styles from "./index.module.scss";
import AuthService from "../../../services/auth.service";
import { Redirect, useHistory } from "react-router";
import LoadingPage from "../../library/Pages/LoadingPage";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { BsGrid } from "react-icons/bs";
import Container from '@material-ui/core/Container';
import SideMenu from "./SideMenu";
import { TiThMenu } from "react-icons/ti";
import Drawer from "@material-ui/core/Drawer";

interface PrivateLayoutProps {
  children: any;
}

function PrivateLayout({ children }: PrivateLayoutProps){

  const history = useHistory();
  const currentUser = useFetch('-', AuthService.getCurrentSession);
  const [menuOpen, setMenuOpen] = useState(false);
    
  const onMenuSelected = () => {
    if(menuOpen) setMenuOpen(false);
  }

  if(currentUser.pending) return <LoadingPage message="Espere por favor..." />
  if(currentUser.error) return <Redirect to="/login"/>

  return (
    <div className={styles.root}>
      <header>
        <AppBar position="static">
          <Toolbar>
          <IconButton edge="start" color="inherit" aria-label="menu" onClick={()=>{
                setMenuOpen(!menuOpen);
              }}>
                <TiThMenu />
              </IconButton>
            <IconButton edge="start" color="inherit" aria-label="menu">
              <BsGrid />
            </IconButton>
            <Typography variant="h6">
              Arbor Trackers
            </Typography>
            <div className={styles.logout}>
              <Button color="inherit" onClick={()=> history.push('/login')}>Logout</Button>
            </div>            
          </Toolbar>
        </AppBar>
      </header>
      <main className={styles.main}>       
          
        <Drawer anchor={'left'} open={menuOpen} onClose={()=>setMenuOpen(false)}>
          <SideMenu onMenuSelected={onMenuSelected} />
        </Drawer>
        
        <Container>
          {children}
        </Container>
        
      </main>

    </div>);
};

export default PrivateLayout;
