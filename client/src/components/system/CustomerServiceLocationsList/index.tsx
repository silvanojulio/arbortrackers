import Drawer from "@material-ui/core/Drawer";
import { useEffect, useState } from "react";
import styles from "./index.module.scss";
import {RiDeleteBin5Line} from "react-icons/ri";
import { ImHome } from "react-icons/im";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { useToasts } from 'react-toast-notifications'
import ServiceLocationService from "../../../services/serviceLocation.service";
import { ICustomer } from "../../../models/customer";
import { useBlockScreen } from "../../library/BlockScreen";
import { IServiceLocation } from "../../../models/serviceLocation";
import EditServiceLocationForm from "../Forms/EditServiceLocationForm";
import CloseButton from "../../library/CloseButton";
import SectionLoader from "../../library/SectionLoader";
import IconButton from "@material-ui/core/IconButton";
import { FaEdit } from "react-icons/fa";
import Grid from "@material-ui/core/Grid";
import ConfirmationDialog from "../../library/ConfirmationDialog";
import { FaChevronRight } from "react-icons/fa";

interface CustomerServiceLocationsListProps {
  show: boolean;
  onClose: ()=>void;
  customer: ICustomer;
  onServiceLocationSelected?: (serviceLocation: IServiceLocation) => void;
}

function CustomerServiceLocationsList(props: CustomerServiceLocationsListProps){

  const [serviceLocations, setServiceLocations] = useState<IServiceLocation[]>([]);
  const [selectedServiceLocation, setSelectedServiceLocation] = useState<IServiceLocation | undefined>(undefined);
  const [showEditServiceLocationForm, setShowEditServiceLocationForm] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);
  const [confirmationOpen, setConfirmationOpen] = useState(false);
  const { addToast } = useToasts();
  const blockScreen = useBlockScreen();

  const init = async () => {
    await setLoading(true);
    const items = await ServiceLocationService.getByCustomerId(props.customer._id || '');
    setServiceLocations(items);
    setLoading(false);

    if(items?.length === 0){
      onNewServiceLocation();
      addToast("Please create a Service Location for the customer",
      {
        appearance: 'warning',
        autoDismiss: true,
      })
    }
  }

  useEffect(()=>{
    init();
  }, [props.customer]);

  const onNewServiceLocation = () => {
    setSelectedServiceLocation(undefined);
    setShowEditServiceLocationForm(true);
  }

  const onEditServiceLocation = (item: IServiceLocation) => {
    setSelectedServiceLocation(item);
    setShowEditServiceLocationForm(true);
  }

  const onRemoveServiceLocation = (item: IServiceLocation) => {
    setSelectedServiceLocation(item);
    setConfirmationOpen(true);
  }

  const onServiceLocationDeleteConfirmed = async () => {
    try {      
      blockScreen(true, "Please wait while the item is being deleted...")
      setConfirmationOpen(false);
      if(selectedServiceLocation?._id){
        await ServiceLocationService.deleteServiceLocation(selectedServiceLocation._id);
        await init(); 
      }
    } catch (error: any) {
      
    }finally{
      blockScreen(false);
    }
  };

  return (
    <Drawer 
      anchor={'left'}
      open={props.show} 
      variant="persistent"
      onClose={props.onClose}>
      
      <div className={styles.root} >

      <CloseButton onClose={props.onClose} />

      <Typography className={styles.title} variant="h5" component="h5">
          <ImHome/>
          Service locations
        </Typography>

        <Typography className={styles.customerTitle} variant="h5" component="h5">          
          Customer: <strong>{props.customer.fullName}</strong>
        </Typography>

        <Button variant="outlined" color="primary"
          onClick={onNewServiceLocation} className={styles.addCustomer}>
          Add Service Location
        </Button>

        <div className={styles.list}>
          {!loading && 
            serviceLocations.map(item => (
              <div className={styles.item}>
                
               <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <strong>{item.address} ({item.cp}){item.isMain ? " [main]" : ""}</strong>
                  </Grid>
                  <Grid item xs={6}>
                    State: <strong>{item.state}</strong>
                  </Grid>
                  <Grid item xs={6}>
                    City: <strong>{item.city}</strong>
                  </Grid>

                  <Grid item xs={12}>
                    Contact: <strong>{item.contact}</strong>
                  </Grid>
                  <Grid item xs={4}>
                    <strong>{item.phone}</strong>
                  </Grid>
                  <Grid item xs={8}>
                     <strong>{item.email}</strong>
                  </Grid>
               </Grid>

              {
                !props.onServiceLocationSelected && (
                  <div>
                    <IconButton onClick={()=>{onEditServiceLocation(item)}} color="primary">
                      <FaEdit />
                    </IconButton>

                    <IconButton onClick={()=>{onRemoveServiceLocation(item)}} color="secondary">
                      <RiDeleteBin5Line />
                    </IconButton>
                  </div>
                )
              }
              {props.onServiceLocationSelected && (
                <div className={styles.selectIcon}>
                  <IconButton 
                    onClick={()=>{props.onServiceLocationSelected && props.onServiceLocationSelected(item)}} 
                    color="primary">
                    <FaChevronRight />
                  </IconButton>
                </div>
              )}
              </div>
            ))
          }

          <SectionLoader show={loading} message="Loading customer's service locations"/>

          { !loading && serviceLocations.length === 0 &&
            <h3>
              -- No Service Locatios --
            </h3>
          }
        </div>

        <EditServiceLocationForm 
          key={`form-${selectedServiceLocation?._id}-${showEditServiceLocationForm}`}
          show={showEditServiceLocationForm} 
          customer={props.customer}
          serviceLocation={selectedServiceLocation} 
          onClose={()=>setShowEditServiceLocationForm(false)}
          onCreated={init}/>

        <ConfirmationDialog title="Delete Service Location" open={confirmationOpen} 
          onCancel={()=>setConfirmationOpen(false)}
          onAccept={onServiceLocationDeleteConfirmed}>
          Are you sure you want to delete the selected service location?
        </ConfirmationDialog>

      </div>

    </Drawer>
    );
};

export default CustomerServiceLocationsList;
