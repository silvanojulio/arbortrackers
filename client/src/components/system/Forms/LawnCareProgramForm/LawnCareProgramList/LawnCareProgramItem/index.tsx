import { Checkbox, FormControlLabel, TextField, Typography } from "@material-ui/core";
import React from "react";
import { IBaseProgramEstimateItem, IProgramItem } from "../../../../../../models/lawnCareProgramTemplate";
import styles from "./index.module.scss";

interface ILawnCareProgramItem {
  item: IProgramItem;
  data: IBaseProgramEstimateItem;
  onChange: (code: string, field: string, newValue:any) => void;
}

function LawnCareProgramItem({item, data, onChange}: ILawnCareProgramItem){
  return (
    <div className={styles.root}>
      <div className={styles.itemTitleRow}>
        {true &&
            <div className={styles.itemSelect}>                
              <FormControlLabel
                control={<Checkbox checked={data?.checked} onClick={()=>onChange(item.code, "checked", !data?.checked)}/>}
                label=""
              />
            </div>}
        <div className={styles.itemTitle}>
          <div>{item.title}</div>
          <div> 
            <small>{item.subtitle}</small>
          </div>
        </div>
      </div>

      <div className={styles.itemDescriptionRow}>             
        <div>{item.description}</div>
        <div>
          <Typography variant="caption" display="block" gutterBottom>
          {item.explanation}
          </Typography>
        </div>
      </div>

      {item.extraOptions && data?.checked && (
          <div>
            {
              item.extraOptions.map((extraOption, index) => (
                <div key={`item-${index}`}>
                  <FormControlLabel
                    control={<Checkbox checked={data?.extraOptions?.includes(extraOption)} onChange={()=>{
                      
                      let newExtraOptions: string[] = data?.extraOptions || [];
                      if(data?.extraOptions?.includes(extraOption)){
                        newExtraOptions = data.extraOptions.filter(x=>x!==extraOption);
                      }else{
                        newExtraOptions.push(extraOption);
                      }
                      onChange(item.code, "extraOptions", newExtraOptions);
                    }}/>}
                    label={extraOption}
                  />              
                </div>
              ))
            }
          </div>
        )}
     {data?.checked && 
     <div className={styles.itemInputs}>
        {
          item.applications && 
          <div>
            <TextField className={styles.applicationsField}     
            onChange={(e) => {
              if(parseInt(e.target.value))
                onChange(item.code, "applications", parseInt(e.target.value))
            }}
            value={data?.applications}        
            label="Applications"/>
          </div>
        }

        <div>
         {item.showCost &&
            <TextField className={styles.costField}
            onChange={(e) => {
              if(parseInt(e.target.value))
                onChange(item.code, "cost", parseInt(e.target.value))
            }}
            value={data?.cost}
            label="Cost"/>
         }
        </div>
       
      </div>}

    </div>
);
};

export default LawnCareProgramItem;
