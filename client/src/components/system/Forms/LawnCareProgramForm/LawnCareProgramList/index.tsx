import React from "react";
import { IBaseProgramEstimateItem, IProgramItem } from "../../../../../models/lawnCareProgramTemplate";
import styles from "./index.module.scss";
import LawnCareProgramItem from "./LawnCareProgramItem";

export interface ILawnCareProgramList {
  items: IProgramItem[];
  data: any;
  onChange: (newData: any) => void;
}

function LawnCareProgramList({items, data, onChange}: ILawnCareProgramList){
  const onItemChange = (code: string, field: string, newValue:any) => {
    const newData = {
      ...data,
      [code]: {
        ...data[code],
        [field]: newValue
      }
    };
    onChange(newData);
  }

  return (
    <div className={styles.root}>
      {
        items.map(item=>(<LawnCareProgramItem 
            key={item.code} 
            onChange={onItemChange}
            data={data[item.code] as IBaseProgramEstimateItem}
            item={item}/>))
      }
    </div>)
};

export default LawnCareProgramList;
