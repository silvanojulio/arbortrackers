import { Button, Checkbox, Step, StepLabel, Stepper, TextField, Typography } from "@material-ui/core";
import { IBaseProgramEstimateItem, ILawnCareProgramEstimate, ILawnCareProgramTemplate, IProgramItem } from "../../../../models/lawnCareProgramTemplate";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import styles from "./index.module.scss";
import { useState } from "react";
import { IServiceLocation } from "../../../../models/serviceLocation";
import { ICustomer } from "../../../../models/customer";
import CustomerSelection from "../../CustomerSelection";
import CustomerServiceLocationsList from "../../CustomerServiceLocationsList";
import LawnCareProgramList from "./LawnCareProgramList";
import LawnCareProgramService from "../../../../services/lawnCareProgram.service";
import { useBlockScreen } from "../../../library/BlockScreen";

interface ILawnCareProgramFormProps {
  template: ILawnCareProgramTemplate;
  onConfirm: () => void;
  onCancel: () => void;
}

const steps = ["Customer", "Lawn Care Program", "Supplemental Services"];

function LawnCareProgramForm({ template, onCancel, onConfirm }: ILawnCareProgramFormProps){  

  const blockScreen = useBlockScreen();
  
  const [currentStep, setCurrentStep] = useState<number>(0);
  const [totalMoney, setTotalMoney] = useState<number>(0);

  const [customerSelectionOpen, setCustomerSelectionOpen] = useState<boolean>(false);
  const [locationSelectionOpen, setLocationSelectionOpen] = useState<boolean>(false);

  //=== >> Form data
  const [selectedCustomer, setSelectedCustomer] = useState<ICustomer>();
  const [selectedLocation, setSelectedLocation] = useState<IServiceLocation>();
  const [baseProgramItems, setBaseProgramItems] = useState({});
  const [supplementalServiceItems, setSupplementalServiceItems] = useState({});
  const [sendEmail, setSendEmail] = useState<boolean>(false);
  //===
    
  const onSelectCustomer = (customer:ICustomer)=>{
    setSelectedCustomer(customer);
    setCustomerSelectionOpen(false);
    setLocationSelectionOpen(true);
  };
  
  const onSelectLocation = (location: IServiceLocation) => {
    setSelectedLocation(location);
    setLocationSelectionOpen(false);
  }

  const onBaseProgramChanges = (newData: any) => {
    const newTotal = getTotal(newData, supplementalServiceItems);
    setTotalMoney(newTotal);
    setBaseProgramItems(newData);
  };

  const onSuplementalProgramChanges = (newData: any) => {
    const newTotal = getTotal(baseProgramItems, newData);
    setTotalMoney(newTotal);
    setSupplementalServiceItems(newData);
  };

  const getTotal = (baseP: any, supplementalP: any) => {

    const fieldsBaseP = Object.keys(baseP);
    const fieldsSupplementalP = Object.keys(supplementalP);
    let total = 0;

    fieldsBaseP.forEach((field: string) => {
      const item = baseP[field] as IBaseProgramEstimateItem;
      if(item.checked) total += item.cost || 0;
    });

    fieldsSupplementalP.forEach((field: string) => {
      const item = supplementalP[field] as IBaseProgramEstimateItem;
      if(item.checked) total += item.cost || 0;
    });

    return total;
  }

  const isNextButtonDisable = () => {

    if(currentStep === 0){
      return !selectedCustomer || !selectedLocation;
    }

    return false;
  };

  const onConfirmForm = async () => {

    try {
      if(!selectedCustomer || !selectedLocation) throw Error("No customer selected");
      blockScreen(true, `Wait while saving the Lawn Care Program estimate.${sendEmail? " Sending email...":""}`);
      await LawnCareProgramService.createEstimate(
        new Date(),
        selectedCustomer._id!,
        selectedLocation._id!,
        baseProgramItems,
        supplementalServiceItems,
        sendEmail
      );

      onConfirm();
    } catch (error: any) {
      console.error(error);
    } finally {
      blockScreen(false);
    }
  };

  return (
    <div className={styles.root}>
      <div className={styles.steps}>
        <Stepper activeStep={currentStep} alternativeLabel>
          {steps.map((label) => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
            </Step>
          ))}
        </Stepper>
      </div>

      {
        currentStep === 0 && (
          <div>
            <Button variant="contained" color="primary"
              className={styles.selectCustomerBtn}
              onClick={()=>{
                setCustomerSelectionOpen(true);
              }}>
              Select customer
            </Button>

            {selectedCustomer && selectedLocation && (
              <div>
                <div>
                  Customer: <strong>{selectedCustomer?.fullName}</strong>
                </div>
                <div>
                  Phone: <strong>{selectedCustomer?.phone}</strong>
                </div>
                <div>
                  Email: <strong>{selectedCustomer?.email}</strong>
                </div>
                <br/>
                <div>
                  Location: 
                </div>
                <div>
                  <strong>{selectedLocation?.address} ({selectedLocation?.cp})</strong>
                </div>
                <div>
                  <strong>{selectedLocation?.state}</strong>
                </div>
                <div>
                  <strong>{selectedLocation?.city}</strong>
                </div>
              </div>
            )}
          </div>
        )
      }

      {currentStep === 1 && (
          <div>            
            <Typography variant="h5" component="h5" className={styles.title}>{template.title}</Typography>
            <p className={styles.description}>{template.descripcion}</p>
            <Typography variant="h6" component="h6" className={styles.title}>BASE PROGRAM</Typography>
        
            <LawnCareProgramList 
              items={template.baseProgram.items} 
              data={baseProgramItems} 
              onChange={onBaseProgramChanges}/>
          </div>
        )
      }
      
      {currentStep === 2 && (
        <LawnCareProgramList items={template.suplementalProgram.items} 
          data={supplementalServiceItems} 
          onChange={onSuplementalProgramChanges}/>
      )}

      {currentStep === 3 && (
          <div>
            <Typography variant="h5" component="h5" className={styles.confirmationTitle}>{template.title}</Typography>
            <Typography variant="h6" component="h6" className={styles.confirmationTitle}>Confirmation</Typography>
            <br/>
            <br/>
            <Typography variant="h6" component="h6" className={styles.title}>Total: $ {totalMoney.toLocaleString()}</Typography>
            <br/>
            <div>
              Email: <strong>{selectedCustomer?.email}</strong>
            </div>
            <div>                
              <FormControlLabel
                control={<Checkbox onClick={()=>setSendEmail(!sendEmail)}/>}
                label={"Send email after confirmation"}
              />              
            </div>
          </div>
        )
      }

      <div className={styles.actionButtons}>  
        {currentStep === 0 && 
        <Button variant="contained" onClick={onCancel} className={styles.backBtn}>
          Cancel
        </Button>}

        {currentStep > 0 && 
        <Button variant="contained" 
          onClick={()=>{
            setCurrentStep(currentStep-1);
            window.scrollTo(0, 0);
          }} className={styles.backBtn}>
          Back
        </Button>}

        {currentStep < steps.length && 
        <Button variant="contained" color="primary" 
          disabled={isNextButtonDisable()}
          onClick={()=>{
            setCurrentStep(currentStep+1);
            window.scrollTo(0, 0);
          }} className={styles.nextBtn}>
          Next
        </Button>}

        {currentStep === steps.length && 
        <Button variant="contained" color="primary" 
          
          onClick={onConfirmForm} className={styles.nextBtn}>
            Confirm
        </Button>}
      </div>

      <CustomerSelection show={customerSelectionOpen}  
        onSelected={onSelectCustomer}
        onClose={() => {
          setCustomerSelectionOpen(false);
        }}/>

      {selectedCustomer && <CustomerServiceLocationsList 
        key={`service-locations-${selectedCustomer?._id}`}
        customer={selectedCustomer} 
        show={locationSelectionOpen}
        onClose={()=>setLocationSelectionOpen(false)} 
        onServiceLocationSelected={onSelectLocation}/>}
    </div>);
};

export default LawnCareProgramForm;
