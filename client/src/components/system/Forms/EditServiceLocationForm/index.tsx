import Drawer from "@material-ui/core/Drawer";
import React, { useState } from "react";
import styles from "./index.module.scss";
import {ImHome} from "react-icons/im";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { useForm } from "react-hook-form";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useToasts } from 'react-toast-notifications'
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import ServiceLocationService from "../../../../services/serviceLocation.service";
import { ICustomer } from "../../../../models/customer";
import { useBlockScreen } from "../../../library/BlockScreen";
import { IServiceLocation } from "../../../../models/serviceLocation";

interface EditServiceLocationFormProps {
  show: boolean;
  onClose: ()=>void;
  onCreated?: ()=>void;
  customer: ICustomer;
  serviceLocation?: IServiceLocation;
}

const locationFormSchema = Yup.object().shape({
  address: Yup.string().required("Required"),
  state: Yup.string().required("Required"),
  city: Yup.string().required("Required"),
  cp: Yup.string().required("Required"),
  email: Yup.string().email(),
  phone: Yup.string(),
  contact: Yup.string(),
});

function EditServiceLocationForm(props: EditServiceLocationFormProps){

  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(locationFormSchema),
  });
  const blockScreen = useBlockScreen();
  const [isMain, setIsMain] = useState<boolean>(props.serviceLocation?.isMain || false);
  
  const { addToast } = useToasts();

  const onSave = async (data: any) => {

    try {
      blockScreen(true, "Saving customer service location ...");

      const entity: IServiceLocation = {
        ...data,
        isMain: isMain,
        customer_id: props.customer._id,
        _id: props.serviceLocation?._id
      };

      if(props.serviceLocation?._id)
        await ServiceLocationService.update(entity);
      else
        await ServiceLocationService.create(entity);
        
      addToast("The customer service location was saved",
      {
        appearance: 'success',
        autoDismiss: true,
      })
      
      if(props.onCreated) props.onCreated();

      props.onClose();
    } catch (error: any) {
      addToast("An error has ocurred. "+ error.message,
      {
        appearance: 'success',
        autoDismiss: true,
      })
    } finally{
      blockScreen(false);
    }
 
  };

  return (
    <Drawer 
      variant="persistent"
      anchor={'left'}
      open={props.show} 
      onClose={props.onClose}>
      
      <div className={styles.root} >
        <Typography className={styles.title} variant="h5" component="h5">
          <ImHome/>
          New Service Location
        </Typography>

        <form 
          className={styles.form} 
          autoComplete="off"
          onSubmit={handleSubmit(onSave)}>

          <TextField className={styles.field}             
            label="U.S. State" 
            variant="outlined"             
            inputProps={{
              ref: register,
              name: 'state',
              defaultValue: props.serviceLocation?.state,
            }} 
            error={errors?.state}
            helperText={errors?.state?.message}/>

          <TextField className={styles.field}             
            label="City" 
            variant="outlined"             
            inputProps={{
              ref: register,
              name: 'city',
              defaultValue: props.serviceLocation?.city,
            }} 
            error={errors?.city}
            helperText={errors?.city?.message}/>

          <TextField className={styles.field}             
            label="Address" 
            variant="outlined"
            inputProps={{
              ref: register,
              name: 'address',
              defaultValue: props.serviceLocation?.address,
            }} 
            error={errors?.address}
            helperText={errors?.address?.message}/>
            
          <TextField className={styles.field}             
            label="CP" 
            variant="outlined"             
            inputProps={{
              ref: register,
              name: 'cp',
              defaultValue: props.serviceLocation?.cp,
            }} 
            error={errors?.cp}
            helperText={errors?.cp?.message}/>


          <TextField className={styles.field}             
            label="Person to contact" 
            variant="outlined"             
            inputProps={{
              ref: register,
              name: 'contact',
              defaultValue: props.serviceLocation?.contact,
            }} 
            error={errors?.contact}
            helperText={errors?.contact?.message}/>

          <TextField className={styles.field}             
            label="Phone" 
            variant="outlined"             
            inputProps={{
              ref: register,
              name: 'phone',
              defaultValue: props.serviceLocation?.phone,
            }} 
            error={errors?.phone}
            helperText={errors?.phone?.message}/>

          <TextField className={styles.field}             
            label="Email" 
            variant="outlined"             
            inputProps={{
              ref: register,
              name: 'email',
              defaultValue: props.serviceLocation?.email,
            }} 
            error={errors?.email}
            helperText={errors?.email?.message}/>


          <FormControlLabel
            control={<Checkbox checked={isMain} onChange={()=>setIsMain(!isMain)} />}
            label="It is the main service location"
          />
          
          <div className={styles.actions}>
            <Button variant="contained" type={'submit'} color="primary" >
              Save
            </Button>

            <Button onClick={props.onClose} type={'button'}>
              Cancel
            </Button>
          </div>
        </form>

      </div>

    </Drawer>
    );
};

export default EditServiceLocationForm;
