import Drawer from "@material-ui/core/Drawer";
import React, { useState } from "react";
import styles from "./index.module.scss";
import {RiUserAddFill} from "react-icons/ri";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { useForm } from "react-hook-form";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useToasts } from 'react-toast-notifications'
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import CustomerService from "../../../../services/customer.service";
import { ICustomer } from "../../../../models/customer";
import { useBlockScreen } from "../../../library/BlockScreen";

interface EditCustomerFormProps {
  show: boolean;
  onClose: ()=>void;
  onCreated?: (createdCustomer?: ICustomer)=>void;
  customerToEdit?: ICustomer;
}

const customerFormSchema = Yup.object().shape({
  fullName: Yup.string().required("Required"),
  email: Yup.string().email().required("Required"),
  phone: Yup.string().required("Required"),
});

function EditCustomerForm(props: EditCustomerFormProps){

  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(customerFormSchema),
  });
  const blockScreen = useBlockScreen();
  const [isActive, setIsActive] = useState(props.customerToEdit?.active);
  
  const { addToast } = useToasts();

  const onSave = async (data: any) => {

    try {
      blockScreen(true, "Saving customer ...");

      let createdCustomer: ICustomer | undefined;

      if(props.customerToEdit?._id)
        await CustomerService.updateCustomer({
          ...data,
          active: isActive,
          _id: props.customerToEdit?._id
        });
      else
        createdCustomer = await CustomerService.create({
          ...data,
          active: isActive
        });
        
      addToast("The customer was saved",
      {
        appearance: 'success',
        autoDismiss: true,
      })
      
      if(props.onCreated && createdCustomer) props.onCreated(createdCustomer);
      else props.onClose();

    } catch (error: any) {
      addToast("An error has ocurred. "+ error.message,
      {
        appearance: 'success',
        autoDismiss: true,
      })
    } finally{
      blockScreen(false);
    }
 
  };

  return (
    <Drawer 
      variant="persistent"
      anchor={'right'}
      open={props.show} 
      onClose={props.onClose}>
      
      <div className={styles.root} >
        <Typography className={styles.title} variant="h5" component="h5">
          <RiUserAddFill/>
          New Customer
        </Typography>

        <form 
          className={styles.form} 
          autoComplete="off"
          onSubmit={handleSubmit(onSave)}>

          <TextField className={styles.field}             
            label="Full Name" 
            variant="outlined"
            inputProps={{
              ref: register,
              name: 'fullName',
              defaultValue: props.customerToEdit?.fullName,
            }} 
            error={errors?.fullName}
            helperText={errors?.fullName?.message}/>

          <TextField className={styles.field}             
            label="Email" 
            variant="outlined"             
            inputProps={{
              ref: register,
              name: 'email',
              defaultValue: props.customerToEdit?.email,
            }} 
            error={errors?.email}
            helperText={errors?.email?.message}/>

          <TextField className={styles.field}             
            label="Phone" 
            variant="outlined"             
            inputProps={{
              ref: register,
              name: 'phone',
              defaultValue: props.customerToEdit?.phone,
            }} 
            error={errors?.phone}
            helperText={errors?.phone?.message}/>

          <FormControlLabel
            control={<Checkbox checked={isActive} onChange={()=>setIsActive(!isActive)} />}
            label="Active"
          />
          
          <div className={styles.actions}>
            <Button variant="contained" type={'submit'} color="primary" >
              Save
            </Button>

            <Button onClick={props.onClose} type={'button'}>
              Cancel
            </Button>
          </div>
        </form>

      </div>

    </Drawer>
    );
};

export default EditCustomerForm;
