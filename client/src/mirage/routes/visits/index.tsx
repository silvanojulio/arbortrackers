import { Registry, Server } from "miragejs";
import { AnyFactories, AnyModels } from "miragejs/-types";
import getAll from './get-all.json';
import getOne from './get-one.json';

const serviceLocationPrograms = (server: Server<Registry<AnyModels, AnyFactories>>) => {
  server.get(
    "/visits/:customerId", () => getAll,
    { timing: 2000 }
  );
  server.post(
    "/visits", () => getAll,
    { timing: 2000 }
  );
  server.put(
    "/visits/:id", () => ({}),
    { timing: 2000 }
  );
  server.delete(
    "/visits/:id", () => ({}),
    { timing: 2000 }
  );
};

export default serviceLocationPrograms;
