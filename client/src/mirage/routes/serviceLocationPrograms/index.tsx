import { Registry, Server } from "miragejs";
import { AnyFactories, AnyModels } from "miragejs/-types";
import getAll from './get-all.json';
import getOne from './get-one.json';

const serviceLocationPrograms = (server: Server<Registry<AnyModels, AnyFactories>>) => {
  server.get(
    "/service-location-programs", () => getAll,
    { timing: 2000 }
  );
  server.get(
    "/service-location-programs/:id", () => getOne,
    { timing: 2000 }
  );
  server.post(
    "/service-location-programs", () => getOne,
    { timing: 2000 }
  );
  server.put(
    "/service-location-programs/:id", () => ({}),
    { timing: 2000 }
  );
  server.delete(
    "/service-location-programs/:id", () => ({}),
    { timing: 2000 }
  );
};

export default serviceLocationPrograms;
