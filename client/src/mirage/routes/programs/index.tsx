import { Registry, Server } from "miragejs";
import { AnyFactories, AnyModels } from "miragejs/-types";
import programsAll from './programs-all.json';
import programGetOne from './program-get-one.json';

const routerPrograms = (server: Server<Registry<AnyModels, AnyFactories>>) => {
  server.get(
    "/programs", () => programsAll,
    { timing: 2000 }
  );
  server.get(
    "/programs/:id", () => programGetOne,
    { timing: 2000 }
  );
  server.post(
    "/programs", () => programGetOne,
    { timing: 2000 }
  );
  server.put(
    "/programs/:id", () => ({}),
    { timing: 2000 }
  );
  server.delete(
    "/programs/:id", () => ({}),
    { timing: 2000 }
  );
};

export default routerPrograms;
