
import { Server } from "miragejs";
import routerPrograms from "./routes/programs";

const createServer = (environment: "test" | "development" = "test") => {
  return new Server({
    environment,
    routes() {
      this.urlPrefix = "http://localhost:8080/api";

      routerPrograms(this);
      this.passthrough();
    },
  });
};

export default createServer;
