import { NextFunction, Response, Request } from "express";
import ContextService from '../context/contextService';
import jwt from 'jsonwebtoken';
import config from '../config';
import AppSession from 'entities/appSession';
const contextService = require('request-context');

const SecurityMiddleware = (req: Request, res: Response, next: NextFunction)=>{
    const token = ContextService.getHeaderValue('security-token');
    if(!token){
        console.log("No token....")
        res.status(403).send();
    }else{
        const appSession = jwt.verify(token, config.appSession.secretKey) as AppSession;
        if(new Date(appSession.expiration) > new Date()){
            contextService.set('CURRENT_REQUEST:appSession', appSession);
            next();
        }else{
            console.log("Expired Token...")
            res.status(403).send();
        }
    }
}

export default SecurityMiddleware;
