import { Response, Request, NextFunction } from 'express';
import { AppUserRepository } from 'repositories/appUserRepository';
export class AppError extends Error{
    public static SchemaValidationFailed:AppError=new AppError(800, "Schema Validation Failed");
    public static AlreadyExist:AppError=new AppError(801, "Already exist");
    public static NotAllowed:AppError=new AppError(403, "Not Allowed");
    public static DoesntExist:AppError=new AppError(400, "Some selected element doesn't exist");
    public static WrongCompany:AppError=new AppError(400, "Wrong company");
    public static WrongUserOrPassword:AppError=new AppError(409, "User and password error");
    public static ElementMalformed:AppError=new AppError(500, "Element is malformed");
    public static InternalError:AppError=new AppError(500, "Internal error");
    public static RelatedDataExist:AppError=new AppError(400, "Related data still exist");

    constructor ( public code:number, public message:string, public extradata:any|null=null){
        super(message);
        // Se puede agregar aca los procesos para loguear errores
    }
    public setExtraData = (extradata:any|null):AppError => new AppError(this.code, this.message, extradata);
}

export default function errorHandler(appError: AppError, req:Request, res:Response, next:NextFunction){
    //console.log(appError)
    res.status(appError.code||500).send({
        code:appError.code||500,
        message:appError.message??"error",
        extradata:appError.extradata||{},
        receivedBody:req.body
    });
    next(); 
}
