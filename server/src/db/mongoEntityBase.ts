import { ObjectID } from "mongodb";
export interface IMongoEntityBaseIdField {
  _id?: ObjectID;
}
export interface IMongoEntityBase extends IMongoEntityBaseIdField {
  isNew(): boolean;
}
export class MongoEntityBase implements IMongoEntityBase {
  _id?: ObjectID;
  public isNew(): boolean {
    return this._id === null || this._id === undefined;
  }
}
