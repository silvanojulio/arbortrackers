import {body}   from 'express-validator';

export const fromBodyIsDate = (field:string) => {
    return body(field)
        .custom(value => !isNaN(Date.parse(value)))
        .withMessage(`Wrong date format in ${field}`);
}
