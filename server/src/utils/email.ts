import Mailgun from "mailgun-js";

export const sendTest = () => {

    const mg = Mailgun(
        {
            apiKey: process.env.MAILGUN_API_KEY || "", 
            domain: process.env.MAILGUN_DOMAIN || ""
        });
    
    const data = {
        from: process.env.EMAIL_FROM || "arbortrackers@gmail.com", 
        to: "silvanojulio@gmail.com",
        subject: "Hello",
        text: "Testing some Mailgun awesomness!"
    };
    
    mg.messages().send(data, function (error, body) {
        console.error(error);
        console.log(body);    
    })
    
}

export const sendEmail = (to: string, subject: string, message?: string, html?:string) => {

    const mg = Mailgun(
        {
            apiKey: process.env.MAILGUN_API_KEY || "", 
            domain: process.env.MAILGUN_DOMAIN || ""
        });
    
    const data = {
        from: process.env.EMAIL_FROM || "arbortrackers@gmail.com", 
        to: to,
        subject: subject,
        text: message,
        html,
    };
    
    mg.messages().send(data, function (error, body) {
        console.error(error);
        console.log(body);    
    })
    
}
