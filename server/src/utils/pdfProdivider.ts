export const getPdfUrlForEstimate = (estimateId: string) => {
    const urlPdf = "https://api.html2pdfrocket.com/pdf";
    const urlPdfKey = "c8cb6d7b-8929-479f-8e83-07550fa2b0fa";
    const urlHtml = `https://arbor-trackers-web-epwwv4aipa-uw.a.run.app/api/html/estimate/${estimateId}`;

    const finalUrl = `${urlPdf}?value=${urlHtml}&apikey=${urlPdfKey}`;
    console.log(finalUrl);
    return finalUrl;
}
