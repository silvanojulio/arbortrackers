import {
  MongoClient,
  ObjectID,
  FilterQuery,
  OptionalId,
  WithId,
} from "mongodb";
import { IMongoEntityBaseIdField } from "db/mongoEntityBase";
import config from "../config";

const database = config.database.name;
const url = config.database.mongoUrl || "";

export class RepositoryBase<T extends IMongoEntityBaseIdField> {
  public collectionName: string = "";

  mongoClient?: MongoClient;

  public constructor(collectionName: string) {
    this.collectionName = collectionName;
  }

  async update(item: T): Promise<T> {
    const collection = await this.getCollection();
    await collection!.updateOne({ _id: item._id } as FilterQuery<T>, {
      $set: item as T,
    });
    this.disconnect();
    return item;
  }

  async insert(item: OptionalId<T>): Promise<WithId<T>> {
    const collection = await this.getCollection();
    const res = await collection!.insertOne({
      ...item,
      _id: new ObjectID().toHexString(),
    });
    this.disconnect();
    return res.ops[0];
  }

  async insertMany(items: OptionalId<T>[]): Promise<WithId<T>[]> {
    const collection = await this.getCollection();
    const res = await collection!.insertMany(
      items.map((item) => {
        return { ...item, _id: new ObjectID().toHexString() };
      })
    );
    this.disconnect();
    return res.ops;
  }

  async getById(id: string | ObjectID): Promise<WithId<T> | null> {
    const collection = await this.getCollection();
    const items = await collection!.findOne<WithId<T>>({
      _id: id,
    } as FilterQuery<T>);
    this.disconnect();
    return items;
  }

  async getByIds(ids: string[]): Promise<WithId<T>[]> {
    const collection = await this.getCollection();
    const items = await collection!
      .find<WithId<T>>({ _id: { $in: ids } } as FilterQuery<T>, {})
      .toArray();
    this.disconnect();
    return items;
  }

  async getManyByFilter(filter: FilterQuery<T>): Promise<WithId<T>[]> {
    const collection = await this.getCollection();
    const items = await collection!.find<WithId<T>>(filter, {}).toArray();
    this.disconnect();
    return items;
  }

  async getByFilter(filter: FilterQuery<T>): Promise<WithId<T> | null> {
    const collection = await this.getCollection();
    const items = await collection!.findOne<WithId<T>>(filter);
    this.disconnect();
    return items;
  }

  async getAll(): Promise<WithId<T>[]> {
    const collection = await this.getCollection();
    const items = await collection!.find<WithId<T>>({}, {}).toArray();
    this.disconnect();
    return items;
  }

  async delete(item: T): Promise<T> {
    const collection = await this.getCollection();
    await collection!.deleteOne({ _id: item._id } as FilterQuery<T>);
    this.disconnect();
    return item;
  }

  private async connect() {
    this.mongoClient = await MongoClient.connect(url, {
      useUnifiedTopology: true,
    });
  }

  private async disconnect() {
    await this.mongoClient?.close();
  }

  public async getCollection() {
    await this.connect();
    const db = this.mongoClient!.db(database);
    const collection = db!.collection<T>(this.collectionName);
    return collection;
  }
}
