import { RepositoryBase } from './repositoryBase';
import { AppUser } from 'entities/appUser';

export class AppUserRepository extends RepositoryBase<AppUser>{

    public constructor() {
        super("AppUsers");
    }

    public async getUserByEmail(email: string): Promise<AppUser | null>{
        return (await this.getByFilter({email}));
    }
}
