import { RepositoryBase } from './repositoryBase';
import { Customer } from 'entities/customer';

export class CustomerRepository extends RepositoryBase<Customer>{

    public constructor() {
        super("Customers");
    }

}
