import { RepositoryBase } from "./repositoryBase";
import { LawnCareProgramEstimate } from "entities/lawnCareProgram";

export class LawnCareProgramEstimateRepository extends RepositoryBase<LawnCareProgramEstimate> {
  public constructor() {
    super("LawnCareProgramEstimates");
  }

  public async getEstimatesView(id?: string) {
    let filter = id
      ? {
          _id: id,
        }
      : {};

    const collection = await this.getCollection();

    const items = await collection
      .aggregate()
      .match(filter)
      .lookup({
        from: "Customers",
        localField: "customer_id",
        foreignField: "_id",
        as: "customer",
      })
      .unwind("$customer")
      .lookup({
        from: "ServiceLocations",
        localField: "serviceLocation_id",
        foreignField: "_id",
        as: "serviceLocation",
      })
      .unwind("$serviceLocation")
      .sort({
        date: -1,
      })
      .toArray();

    return items;
  }

  public async getEstimateViewById(id?: string) {
    const items = await this.getEstimatesView(id);
    return items.length > 0 ? items[0] : null;
  }
}
