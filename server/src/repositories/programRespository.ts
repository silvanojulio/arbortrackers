import { RepositoryBase } from "./repositoryBase";
import { Program } from "entities/program";

export class ProgramRepository extends RepositoryBase<Program> {
  public constructor() {
    super("Programs");
  }
}
