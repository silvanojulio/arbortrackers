import { ServiceLocationProgramVisitView } from "entities/views/serviceLocationProgramVisitView";
import { ServiceLocationProgramVisit } from "./../entities/serviceLocationProgramVisit";
import { RepositoryBase } from "./repositoryBase";

export class ServiceLocationProgramVisitRepository extends RepositoryBase<ServiceLocationProgramVisit> {
  public constructor() {
    super("ServiceLocationProgramsVisits");
  }

  public async getVisits(status: string, from: Date, to: Date) {
    let filter = {
      $match: {
        estimatedDate: {
          $gte: from,
          $lt: to,
        },
      },
    } as any;

    if (status) {
      filter.$match.status = status;
    }
    const agg = [
      filter,
      {
        $lookup: {
          from: "ServiceLocationPrograms",
          localField: "serviceLocationProgramId",
          foreignField: "_id",
          as: "serviceLocationProgram",
        },
      },
      {
        $unwind: {
          path: "$serviceLocationProgram",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "Programs",
          localField: "serviceLocationProgram.programId",
          foreignField: "_id",
          as: "program",
        },
      },
      {
        $unwind: {
          path: "$program",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "ServiceLocations",
          localField: "serviceLocationProgram.serviceLocationId",
          foreignField: "_id",
          as: "serviceLocation",
        },
      },
      {
        $unwind: {
          path: "$serviceLocation",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "Customers",
          localField: "serviceLocation.customer_id",
          foreignField: "_id",
          as: "customer",
        },
      },
      {
        $unwind: {
          path: "$customer",
          preserveNullAndEmptyArrays: true,
        },
      },
    ];

    const collection = await this.getCollection();

    const items = (await collection
      .aggregate(agg)
      .sort({ estimatedDate: -1 })
      .toArray()) as any[];

    return items as ServiceLocationProgramVisitView[];
  }
}
