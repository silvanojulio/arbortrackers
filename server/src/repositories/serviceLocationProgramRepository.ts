import { ServiceLocationProgram } from "./../entities/serviceLocationProgram";
import { RepositoryBase } from "./repositoryBase";
import { ServiceLocationProgramView } from "./../entities/views/serviceLocationProgramView";

export class ServiceLocationProgramRepository extends RepositoryBase<ServiceLocationProgram> {
  public constructor() {
    super("ServiceLocationPrograms");
  }
  public async getPrograms(status: string, from: Date, to: Date) {
    let filter = {
      $match: {
        $or: [
          {
            validFrom: {
              $gte: from,
              $lt: to,
            },
          },
          {
            validTo: {
              $gte: from,
              $lt: to,
            },
          },
        ],
      },
    } as any;

    if (status) {
      filter.$match.status = status;
    }

    const agg = [
      filter,
      {
        $lookup: {
          from: "Programs",
          localField: "programId",
          foreignField: "_id",
          as: "program",
        },
      },
      {
        $unwind: {
          path: "$program",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "ServiceLocations",
          localField: "serviceLocationId",
          foreignField: "_id",
          as: "serviceLocation",
        },
      },
      {
        $unwind: {
          path: "$serviceLocation",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "Customers",
          localField: "serviceLocation.customer_id",
          foreignField: "_id",
          as: "customer",
        },
      },
      {
        $unwind: {
          path: "$customer",
          preserveNullAndEmptyArrays: true,
        },
      },
    ];
    const collection = await this.getCollection();
    const items = (await collection.aggregate(agg).toArray()) as any[];

    return items as ServiceLocationProgramView[];
  }
}
