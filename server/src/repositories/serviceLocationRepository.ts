import { ServiceLocation } from 'entities/serviceLocation';
import { RepositoryBase } from './repositoryBase';

export class ServiceLocationRepository extends RepositoryBase<ServiceLocation>{

    public constructor() {
        super("ServiceLocations");
    }

    public async getByCustomerId(customerId: string): Promise<ServiceLocation[]>{
        return await this.getManyByFilter({customer_id: customerId});
    }

}
