import { LawnCareProgramEstimateManager } from './../managers/lawnCareProgramEstimateManager';
import express, { Request, Response, NextFunction } from 'express';


var HtmlRouter = express.Router({mergeParams:true});

HtmlRouter.route('/estimate/:id')
.get(async (req: Request, res: Response, next:NextFunction)=>{
    try{
        const htmlContent = await LawnCareProgramEstimateManager.getEstimateHtml(req.params.id);
        
        res.contentType("text/html; charset=UTF-8");
        res.send(htmlContent);

    }catch(error){
        console.error("ERROR PDF", error)

        next(error);
    }
})

export default HtmlRouter;
