import express, { Request, Response, NextFunction } from "express";
import { body } from "express-validator";
import SchemaValidatorMiddleware from "../middlewares/generalSchemaValidator";
import { ServiceLocationManager } from "../managers/serviceLocationManager";
import { IServiceLocation } from "../entities/serviceLocation";

var ServiceLocationRouter = express.Router({ mergeParams: true });

const serviceLocationAddSchema = [
  body("address").isString().isLength({ min: 4 }),
  body("email").optional().isEmail(),
  body("phone").optional().isString(),
  body("city").isString(),
  body("customer_id").isString(),
  body("contact").optional().isString(),
  body("point").optional().isArray(),
  body("isMain").isBoolean(),
  body("cp").isString(),
];

ServiceLocationRouter.route("/")

  .post(serviceLocationAddSchema, SchemaValidatorMiddleware)
  .post(async (req: Request, res: Response, next: NextFunction) => {
    try {
      const data = req.body;
      const serviceLocation = req.body as IServiceLocation;
      const created = await ServiceLocationManager.create(serviceLocation);
      res.status(200).send(created);
    } catch (error) {
      next(error);
    }
  });

ServiceLocationRouter.route("/:id")

  .put(serviceLocationAddSchema, SchemaValidatorMiddleware)
  .put(async (req: Request, res: Response, next: NextFunction) => {
    try {
      const data = req.body;
      const serviceLocation = req.body as IServiceLocation;
      serviceLocation._id = req.params.id as any;
      await ServiceLocationManager.edit(serviceLocation);
      res.status(200).send();
    } catch (error) {
      next(error);
    }
  })

  .delete(async (req: Request, res: Response, next: NextFunction) => {
    try {
      await ServiceLocationManager.delete(req.params.id);
      res.status(200).send();
    } catch (error) {
      next(error);
    }
  });

ServiceLocationRouter.route("/customer/:customerId")
.get(async (req: Request, res: Response, next: NextFunction) => {
  try {
    const items = await ServiceLocationManager.getByCustomerId(
      req.params.customerId
    );
    res.status(200).send(items);
  } catch (error) {
    next(error);
  }
});

export default ServiceLocationRouter;
