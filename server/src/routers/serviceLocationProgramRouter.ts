import express, { Request, Response, NextFunction } from "express";
import { body } from "express-validator";
import SchemaValidatorMiddleware from "../middlewares/generalSchemaValidator";
import {
  IServiceLocationProgram,
  ServiceLocationProgram,
} from "./../entities/serviceLocationProgram";
import { ServiceLocationProgramManager } from "./../managers/serviceLocationProgramManager";

var ServiceLocationProgramRouter = express.Router({ mergeParams: true });

const serviceLocationProgramSchema = [
  body("serviceLocationId").isString().not().isEmpty(),
  body("programId").isString().not().isEmpty(),
  body("programComments").isString(),
  body("validFrom")
    .isString()
    .not()
    .isEmpty()
    .customSanitizer((value) => new Date(value)),
  body("validTo")
    .isString()
    .not()
    .isEmpty()
    .customSanitizer((value) => new Date(value)),
  body("estimatedPrice").isNumeric().not().isEmpty(),
  body("amountOfVisits").isNumeric().not().isEmpty(),
  body("details").isArray(),
];

ServiceLocationProgramRouter.route("/")
  .post(serviceLocationProgramSchema, SchemaValidatorMiddleware)
  .post(async (req: Request, res: Response, next: NextFunction) => {
    try {
      const data = req.body as IServiceLocationProgram;
      const serviceLocationProgram = new ServiceLocationProgram(data);
      const created = await ServiceLocationProgramManager.create(
        serviceLocationProgram
      );
      res.status(200).send(created);
    } catch (error) {
      console.error("Error creating service location program", error);
      next(error);
    }
  });

ServiceLocationProgramRouter.route("/programs").get(
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const programs = await ServiceLocationProgramManager.getPrograms(
        req.query.status as string,
        new Date(req.query.from as string),
        new Date(req.query.to as string));
      res.status(200).send(programs);
    } catch (error) {
      console.error("Error getting service location programs", error);
      next(error);
    }
  }
)

ServiceLocationProgramRouter.route("/visits-all").get(
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const items = await ServiceLocationProgramManager.getVisits(
        req.query.status as string, 
        new Date(req.query.from as string), 
        new Date(req.query.to as string));
      res.status(200).send(items);
    } catch (error) {
      console.error("Error getting service location program visits", error);
      next(error);
    }
  }
);

ServiceLocationProgramRouter.route("/:id")
  .delete(async (req: Request, res: Response, next: NextFunction) => {
    try {
      await ServiceLocationProgramManager.delete(req.params.id);
      res.status(200).send();
    } catch (error) {
      next(error);
    }
  })

  .put(serviceLocationProgramSchema, SchemaValidatorMiddleware)
  .put(async (req: Request, res: Response, next: NextFunction) => {
    try {
      const data = req.body;
      const serviceLocationProgram = new ServiceLocationProgram({
        ...data,
        _id: req.params.id,
      } as IServiceLocationProgram);
      await ServiceLocationProgramManager.update(serviceLocationProgram);
      res.status(200).send();
    } catch (error) {
      next(error);
    }
  });

export default ServiceLocationProgramRouter;
