import fs from 'fs-extra';
import { LawnCareProgramEstimateManager } from './../managers/lawnCareProgramEstimateManager';
import express, { Request, Response, NextFunction } from 'express';
import {body}   from 'express-validator';
import SchemaValidatorMiddleware from '../middlewares/generalSchemaValidator';
import { IBaseProgramEstimateItem, LawnCareProgramEstimate } from '../entities/lawnCareProgram';
import { fromBodyIsDate } from '../utils/validators';

var LawnCareProgramEstimateRouter = express.Router({mergeParams:true});

const getItemsSchema = (field: string) => [
    body(`${field}.*.code`).isString(),
    body(`${field}.*.cost`).optional().isNumeric(),
    body(`${field}.*.applications`).optional().isNumeric(),

    body(`${field}.*.extraOptions`).optional().isArray(),
    body(`${field}.*.extraOptions.*`).isString(),    
]

const lawnCareProgramEstimateAddSchema = [
    fromBodyIsDate('date'),
    body('serviceLocationId').isString(),
    body('customerId').isString(),
    body('sendEmail').isBoolean(),
    
    body('baseProgramItems').isArray(),
    ...getItemsSchema("baseProgramItems"),

    body('supplementalServiceItems').isArray(),
    ...getItemsSchema("supplementalServiceItems"),
];

LawnCareProgramEstimateRouter.route('/')

.get( async (req: Request, res: Response, next:NextFunction)=>{
    try{
        const items = await LawnCareProgramEstimateManager.getList();
        res.status(200).send(items)
    }catch(error){
        console.error(error);
        next(error)
    }
})

.post(lawnCareProgramEstimateAddSchema, SchemaValidatorMiddleware)
.post( async (req: Request, res: Response, next:NextFunction)=>{
    try{
        const data = req.body;
        const programEstimate =  new LawnCareProgramEstimate(
            data.date,
            'active',
            data.serviceLocationId,
            data.customerId,
            data.baseProgramItems as IBaseProgramEstimateItem[],
            data.supplementalServiceItems as IBaseProgramEstimateItem[],
        );

        const created = await LawnCareProgramEstimateManager.create(programEstimate, data.sendEmail);

        res.status(200).send(created)

    }catch(error){
        next(error)
    }
});

LawnCareProgramEstimateRouter.route('/template')

.get((req: Request, res: Response, next:NextFunction)=>{
    try{
        const items = LawnCareProgramEstimateManager.getTemplate();
        res.status(200).send(items)
    }catch(error){next(error)}
})

LawnCareProgramEstimateRouter.route('/pdf/:id')

.get(async (req: Request, res: Response, next:NextFunction)=>{
    try{
        const pdfPath = await LawnCareProgramEstimateManager.getEstimatePdf(req.params.id);
        
        //var data =fs.readFileSync(pdfPath);
        res.contentType("application/pdf");
        res.send(pdfPath);

    }catch(error){
        console.error("ERROR PDF", error)

        next(error);
    }
})

LawnCareProgramEstimateRouter.route('/html/:id')
.get(async (req: Request, res: Response, next:NextFunction)=>{
    try{
        const htmlContent = await LawnCareProgramEstimateManager.getEstimateHtml(req.params.id);
        
        res.contentType("text/html; charset=UTF-8");
        res.send(htmlContent);

    }catch(error){
        console.error("ERROR PDF", error)

        next(error);
    }
})

export default LawnCareProgramEstimateRouter;
