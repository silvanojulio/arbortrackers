import express, { Request, Response, NextFunction } from "express";
import { SecurityManager } from "../managers/securityManager";
import ContextService from "../context/contextService";
import { body } from "express-validator";
import SchemaValidatorMiddleware from "../middlewares/generalSchemaValidator";
import { AppError } from "../middlewares/appErrorMiddleware";
import AppSession from "../entities/appSession";
import config from "../config";
import { AppUser } from "../entities/appUser";
var SessionRouter = express.Router();

const loginSchemaValidatorRules = [
  body("email").isEmail(),
  body("password").isLength({ min: 5 }),
];

const changePassSchemaValidatorRules = [
  body("email").isEmail(),
  body("old_pass").isLength({ min: 5 }),
  body("new_pass").isLength({ min: 5 }),
];

SessionRouter.post(
  "/login",
  loginSchemaValidatorRules,
  SchemaValidatorMiddleware
);
SessionRouter.post(
  "/login",
  async (req: Request, res: Response, next: NextFunction) => {
    SecurityManager.userLogin(req.body.email, req.body.password)
      .then((appSession: AppSession) => res.status(200).send(appSession))
      .catch((appError: AppError) => next(appError));
  }
);

SessionRouter.put(
  "/changepass",
  changePassSchemaValidatorRules,
  SchemaValidatorMiddleware
);
SessionRouter.put(
  "/changepass",
  async (req: Request, res: Response, next: NextFunction) => {
    SecurityManager.userChangePass(
      req.body.email,
      req.body.old_pass,
      req.body.new_pass
    )
      .then((appUser: AppUser) => res.status(200).send(appUser))
      .catch((appError: AppError) => next(appError));
  }
);

SessionRouter.get(
  "/createAdmin",
  async (req: Request, res: Response, next: NextFunction) => {
    SecurityManager.createUserAdminIfNotExists()
      .then(() => res.status(200).send())
      .catch((appError: AppError) => next(appError));
  }
);

SessionRouter.get(
  "/user",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = ContextService.getCurrentUser();
      res.status(200).send(user);
    } catch (error: any) {
      next(AppError.InternalError.setExtraData({ error: error?.message }));
    }
  }
);

SessionRouter.get("/config", async (req: Request, res: Response) => {
  try {
    res.status(200).send({
      timeZone: config.publicConfig.timezone,
      companyName: config.publicConfig.companyName,
    });
  } catch (error: any) {
    console.error(JSON.stringify(error));
    res.status(409).send(error);
  }
});

SessionRouter.get(
  "/",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const session = ContextService.getCurrentSession();
      res.status(200).send(session);
    } catch (error: any) {
      next(AppError.InternalError.setExtraData({ error: error?.message }));
    }
  }
);

export default SessionRouter;
