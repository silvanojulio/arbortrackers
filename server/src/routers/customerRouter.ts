import { CustomerManager } from "./../managers/customerManager";
import express, { Request, Response, NextFunction } from "express";
import { body } from "express-validator";
import SchemaValidatorMiddleware from "../middlewares/generalSchemaValidator";
import { Customer } from "../entities/customer";

var CustomerRouter = express.Router({ mergeParams: true });

const customerAddSchema = [
  body("fullName").isString().isLength({ min: 4 }),
  body("email").isEmail(),
  body("phone").isString(),
];

CustomerRouter.route("/")
  .get(async (req: Request, res: Response, next: NextFunction) => {
    try {
      const items = await CustomerManager.getList();
      res.status(200).send(items);
    } catch (error) {
      next(error);
    }
  })

  .post(customerAddSchema, SchemaValidatorMiddleware)
  .post(async (req: Request, res: Response, next: NextFunction) => {
    try {
      const data = req.body;
      const customer = new Customer(
        data.fullName,
        data.active,
        data.email,
        data.phone
      );
      const created = await CustomerManager.create(customer);
      res.status(200).send(created);
    } catch (error) {
      next(error);
    }
  });

CustomerRouter.route("/:id")
  .delete(async (req: Request, res: Response, next: NextFunction) => {
    try {
      await CustomerManager.delete(req.params.id);
      res.status(200).send();
    } catch (error) {
      next(error);
    }
  })
  .put(customerAddSchema, SchemaValidatorMiddleware)
  .put(async (req: Request, res: Response, next: NextFunction) => {
    try {
      const data = req.body;
      const customer = new Customer(
        data.fullName,
        data.active,
        data.email,
        data.phone
      );
      customer._id = req.params.id as any;
      const created = await CustomerManager.edit(customer);
      res.status(200).send(created);
    } catch (error) {
      next(error);
    }
  });

export default CustomerRouter;
