import { ILawnCareProgramTemplate } from "./../entities/templates";
import {
  IBaseProgramEstimateItem,
  ILawnCareProgramEstimateView,
  LawnCareProgramEstimate,
} from "../entities/lawnCareProgram";
import { LawnCareProgramEstimateRepository } from "../repositories/lawnCareProgramEstimateRepository";
import { CustomerRepository } from "../repositories/customerRepository";
import { AppError } from "../middlewares/appErrorMiddleware";
import { ServiceLocationRepository } from "../repositories/serviceLocationRepository";
import dataTemplate from "../data/lawncareprogramtemplate.json";
import ContextService from "../context/contextService";
import hbs from "handlebars";
import fs from "fs-extra";
import path from "path";
import {
  EstimateItemPdfModel,
  LawnCareEstimatePdfModel,
} from "entities/pdfModels/lawnCareProgramEstimate";
import axios from "axios";
import qs from "qs";
import { sendEmail } from "../utils/email";
import { getPdfUrlForEstimate } from "../utils/pdfProdivider";

export class LawnCareProgramEstimateManager {
  static programManager: LawnCareProgramEstimateRepository =
    new LawnCareProgramEstimateRepository();

  public static async create(
    program: LawnCareProgramEstimate,
    sendEmailAfter: boolean
  ) {
    const loggedUser = ContextService.getCurrentUser();
    const customerRepo = new CustomerRepository();
    const customer = await customerRepo.getById(program.customer_id);
    if (!customer) throw new AppError(404, "Customer does not exists");

    const serviceLocationRepo = new ServiceLocationRepository();
    const serviceLocation = serviceLocationRepo.getById(
      program.serviceLocation_id
    );
    if (!serviceLocation) throw new AppError(404, "Location does not exists");

    program.userCreateId = loggedUser._id;

    const insertResult = await this.programManager.insert(program);

    try {
      if (customer.email) {
        this.sendEstimateEmail(insertResult._id, customer.email);
      }
    } catch (err) {
      console.error("No email sent", err);
    }

    return insertResult;
  }

  public static async edit(program: LawnCareProgramEstimate) {
    return await this.programManager.update(program);
  }

  public static async getList(): Promise<ILawnCareProgramEstimateView[]> {
    return (await this.programManager.getEstimatesView()) as ILawnCareProgramEstimateView[];
  }

  public static getTemplate() {
    return dataTemplate;
  }

  public static async getEstimateHtml(estimateId: string) {
    const estimate = (await this.programManager.getEstimateViewById(
      estimateId
    )) as ILawnCareProgramEstimateView;

    const template = this.getTemplate();
    const pdfModel: LawnCareEstimatePdfModel = {
      total: getEstimateTotal(estimate).toLocaleString(),

      header: {
        programType: getEstimateType(estimate),
        date: new Date(estimate.date).toDateString(),
        customerName: estimate.customer.fullName,
        customerAddress: estimate.serviceLocation.address,
        customerEmail: estimate.customer.email,
        customerPhone: estimate.customer.phone,
      },

      baseProgramItems: getEstimateItems(
        template,
        estimate.baseProgramItems.filter((x) => x.code !== "TYPE")
      ),
      supplementalServicesItems: getEstimateItems(
        template,
        estimate.supplementalServiceItems
      ),

      footer: {
        user: "Renzo Giovanni",
        phone: "(203) 898-0898",
        email: "arbortrackers@gmail.com",
      },
    };

    const content = await compileHtml(
      "lawnCareProgramEstimateTemplate",
      pdfModel
    );
    return content;
  }

  public static async getEstimatePdf(estimateId: string) {
    const content = await this.getEstimateHtml(estimateId);

    console.log(content);

    const resp = await generatePdfFromProvider(content);

    return resp.data;
  }

  public static async sendEstimateEmail(estimateId: any, email: string) {
    const pdfLink = getPdfUrlForEstimate(estimateId);
    const emailContent = await compileHtml("estimateEmail", {
      pdfLink,
    });

    sendEmail(email, "ArborTrackers Estimate", undefined, emailContent);
  }
}

const getEstimateType = (estimate: ILawnCareProgramEstimateView): string => {
  const typeItem = estimate.baseProgramItems.find((x) => x.code === "TYPE");
  return typeItem?.extraOptions?.find((x) => true) || "-";
};

const getSubTotalCost = (cost?: number, applications?: number) => {
  if (cost) {
    return cost; //* (applications || 1);
  }
  return 0;
};
const getEstimateTotal = (estimate: ILawnCareProgramEstimateView): number => {
  let total = 0;

  estimate.baseProgramItems.forEach(
    (item) => (total += getSubTotalCost(item.cost, item.applications))
  );
  estimate.supplementalServiceItems.forEach(
    (item) => (total += getSubTotalCost(item.cost, item.applications))
  );

  return total;
};

const getEstimateItems = (
  template: any,
  items?: IBaseProgramEstimateItem[]
): EstimateItemPdfModel[] => {
  if (!items) return [];

  let resultItems: EstimateItemPdfModel[] = [];
  const itemsTemplate = [
    ...template.baseProgram.items,
    ...template.suplementalProgram.items,
  ];

  items.forEach((item) => {
    const itemTemplate = itemsTemplate.find((x) => x.code === item.code);

    let itemResult: EstimateItemPdfModel = {
      title: itemTemplate.title,
      subtitle: itemTemplate.subtitle,
      description: itemTemplate.description,
      explanation: itemTemplate.explanation,
      totalItem: getSubTotalCost(item.cost, item.applications).toLocaleString(),

      showApplications: !!item.applications,
      applications: item.applications,

      showExtraOptions: !!item.extraOptions,
      extraOptions: item.extraOptions,
    };

    resultItems.push(itemResult);
  });

  return resultItems;
};

const compileHtml = async (template: string, data: any) => {
  const filePath = path.join(
    process.cwd(),
    "src",
    "pdfTemplates",
    template + ".html"
  );
  const html = await fs.readFile(filePath, "utf-8");
  return hbs.compile(html)(data);
};

const generatePdfFromProvider = (content: any) => {
  var apikey = "c8cb6d7b-8929-479f-8e83-07550fa2b0fa";
  var url = "https://api.html2pdfrocket.com/pdf";

  return axios({
    method: "post",
    url,
    responseType: "blob",
    headers: {
      "content-type": "application/x-www-form-urlencoded;charset=utf-8",
    },
    data: qs.stringify({
      apikey,
      value: encodeURIComponent(content),
    }),
  });
};

const generatePdfFromCloudFunction = (url: string) => {
  const cloudFunctionUrl =
    "https://us-central1-arbortrackers-software.cloudfunctions.net/get-pdf";
  return axios({
    url: `${cloudFunctionUrl}?url=${url}`,
    responseType: "blob",
  });
};
