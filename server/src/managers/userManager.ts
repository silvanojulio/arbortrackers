import { AppUser, Roles } from "../entities/appUser";
import { AppUserRepository } from "../repositories/appUserRepository";
import ContextService from "../context/contextService";
import { AppError } from "../middlewares/appErrorMiddleware";

export class UserManager {
  public static async getList(): Promise<Array<AppUser>> {
    const loggedUser = ContextService.getCurrentUser();
    const appUserRepo = new AppUserRepository();
    return await appUserRepo.getAll();
  }
  public static async getById(id: string): Promise<AppUser> {
    const loggedUser = ContextService.getCurrentUser();
    const appUserRepo = new AppUserRepository();
    const user = await appUserRepo.getById(id);
    if (!user) throw AppError.DoesntExist.setExtraData({ user_id: id });
    return user;
  }
  public static async getActive(): Promise<Array<AppUser>> {
    const loggedUser = ContextService.getCurrentUser();
    const appUserRepo = new AppUserRepository();
    return await appUserRepo.getManyByFilter(
      !Roles.SuperAdmin.isEqual(loggedUser.role)
        ? {
            active: true,
          }
        : {
            active: true,
          }
    );
  }
  public static async addUser(newUser: AppUser): Promise<AppUser> {
    // Solo el usuario SuperAdmin puede crear usuarios CompanyAdmin.
    const loggedUser = ContextService.getCurrentUser();
    if (
      !Roles.Employee.isEqual(newUser.role) &&
      !Roles.SuperAdmin.isEqual(loggedUser.role)
    )
      throw AppError.NotAllowed;

    // Se debe verificar que no exista otro usuario con el mismo mail en la base de datos.
    const appUserRepo: AppUserRepository = new AppUserRepository();
    const userAlreadyExists: AppUser | null = await appUserRepo.getUserByEmail(
      newUser.email
    );
    if (userAlreadyExists !== null)
      throw AppError.AlreadyExist.setExtraData({ newUserEmail: newUser.email });

    return await appUserRepo.insert(newUser);
  }
  public static async updateUser(updatedUser: AppUser): Promise<AppUser> {
    const appUserRepo: AppUserRepository = new AppUserRepository();
    const actualUser: AppUser | null = await appUserRepo.getById(
      updatedUser._id!
    );
    if (actualUser === null)
      throw AppError.DoesntExist.setExtraData({
        user_id: updatedUser._id,
        updateData: updatedUser,
      });

    // Se debe verificar que no exista otro usuario con el mismo mail en la base de datos.
    const userAlreadyExists: AppUser | null = await appUserRepo.getUserByEmail(
      updatedUser.email
    );
    if (
      userAlreadyExists !== null &&
      userAlreadyExists._id !== userAlreadyExists._id
    )
      throw AppError.AlreadyExist.setExtraData({
        updateDataEmail: updatedUser.email,
      });

    return await appUserRepo.update(updatedUser);
  }
  public static async deleteUser(id: string): Promise<AppUser> {
    const appUserRepo: AppUserRepository = new AppUserRepository();
    const userToDelete: AppUser | null = await appUserRepo.getById(id);
    if (userToDelete === null)
      throw AppError.DoesntExist.setExtraData({ user_id: id });

    const loggedUser = ContextService.getCurrentUser();
    if (Roles.Employee.isEqual(loggedUser.role)) throw AppError.NotAllowed;
    return await appUserRepo.delete(userToDelete);
  }
}
