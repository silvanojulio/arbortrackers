import AppSession from "../entities/appSession";
import { AppUser, Roles } from "../entities/appUser";
import { AppUserRepository } from "../repositories/appUserRepository";
import { AppError } from "../middlewares/appErrorMiddleware";
import moment from "moment";
import jwt from "jsonwebtoken";
import config from "../config";
import bcrypt from "bcrypt";

export class SecurityManager {
  public static async userLogin(
    email: string,
    password: string
  ): Promise<AppSession> {
    const appUserRepo = new AppUserRepository();

    const loggedUser = await appUserRepo.getUserByEmail(
      email.toLocaleLowerCase()
    );

    if (loggedUser === null) throw AppError.WrongUserOrPassword;

    if (
      !(await this.compareLiteralAndHash(
        password,
        loggedUser.passwordHash as string
      ))
    )
      throw AppError.WrongUserOrPassword;

    const expiration = moment(moment.now()).add(24, "hours").toDate();
    const token = this.generateSecurityToken(loggedUser, expiration);
    const appSession = new AppSession(loggedUser, expiration, token);
    return appSession;
  }

  public static async userChangePass(
    email: string,
    old_pass: string,
    new_pass: string
  ): Promise<AppUser> {
    const appUserRepo = new AppUserRepository();
    const loggedUser = await appUserRepo.getUserByEmail(
      email.toLocaleLowerCase()
    );

    if (loggedUser === null) throw AppError.DoesntExist;

    if (
      !(await this.compareLiteralAndHash(
        old_pass,
        loggedUser.passwordHash as string
      ))
    )
      throw AppError.WrongUserOrPassword;

    loggedUser.passwordHash = await this.getPasswordHashFromLiteral(new_pass);
    return await appUserRepo.update(loggedUser);
  }

  public static async createUserAdminIfNotExists() {
    const adminUser: AppUser = new AppUser(
      "Admin",
      "admin@admin.com",
      await this.getPasswordHashFromLiteral("Admin2020"),
      true,
      Roles.SuperAdmin
    );
    const appUserRepo = new AppUserRepository();
    const loggedUser = await appUserRepo.getUserByEmail(adminUser.email);

    if (loggedUser !== null)
      throw AppError.AlreadyExist.setExtraData({ adminUser: adminUser });

    await appUserRepo.insert(adminUser);
  }

  public static getAppSessionFromToken(token: string): AppSession {
    const decoded = jwt.verify(
      token,
      config.appSession.secretKey
    ) as AppSession;
    decoded.securityToken = token;
    return decoded;
  }

  private static generateSecurityToken(
    appUser: AppUser,
    expiration: Date
  ): string {
    return jwt.sign({ appUser, expiration }, config.appSession.secretKey);
  }

  public static async getPasswordHashFromLiteral(
    literal: string
  ): Promise<string> {
    return await bcrypt.hash(literal, config.appSession.passwordSaltRounds);
  }

  private static async compareLiteralAndHash(
    literal: string,
    hash: string
  ): Promise<boolean> {
    return await bcrypt.compare(literal, hash);
  }
}
