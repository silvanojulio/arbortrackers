import { ServiceLocationRepository } from "../repositories/serviceLocationRepository";
import { IServiceLocation, ServiceLocation } from "../entities/serviceLocation";

export class ServiceLocationManager {
  static mainRepo: ServiceLocationRepository = new ServiceLocationRepository();

  public static async create(serviceLocation: IServiceLocation) {
    return await this.mainRepo.insert(new ServiceLocation(serviceLocation));
  }

  public static async delete(serviceLocationId: string) {
    const serviceLocation = await this.mainRepo.getById(serviceLocationId);
    if (!serviceLocation) throw new Error("Service location does not exist");
    await this.mainRepo.delete(serviceLocation);
  }

  public static edit(serviceLocation: IServiceLocation) {
    this.mainRepo.update(new ServiceLocation(serviceLocation));
  }

  public static async getList(): Promise<IServiceLocation[]> {
    return await this.mainRepo.getAll();
  }

  public static async getByCustomerId(
    customerId: string
  ): Promise<IServiceLocation[]> {
    return await this.mainRepo.getByCustomerId(customerId);
  }
}
