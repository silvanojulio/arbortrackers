import { ProgramRepository } from "./../repositories/programRespository";
import { ServiceLocation } from "entities/serviceLocation";
import { ServiceLocationProgram } from "./../entities/serviceLocationProgram";
import { ServiceLocationProgramRepository } from "./../repositories/serviceLocationProgramRepository";
import { ServiceLocationRepository } from "./../repositories/serviceLocationRepository";
import { ServiceLocationProgramVisitRepository } from "./../repositories/serviceLocationProgramVisitRepository";
import { ServiceLocationProgramVisit } from "./../entities/serviceLocationProgramVisit";
import {
  ServiceLocationProgramStatus,
  ServiceLocationProgramVisitStatus,
} from "./../entities/enums/serviceLocationProgram";

export class ServiceLocationProgramManager {
  static mainRepo = new ServiceLocationProgramRepository();
  static serviceLocationRepo = new ServiceLocationRepository();
  static serviceLocationProgramVisitRepo =
    new ServiceLocationProgramVisitRepository();
  static programRepo = new ProgramRepository();

  static validateServiceLocation = async (
    newServiceLocationProgram: ServiceLocationProgram
  ) => {
    //1 - Validate serviceLocation exits
    const serviceLocation = await this.serviceLocationRepo.getById(
      newServiceLocationProgram.serviceLocationId
    );
    if (!serviceLocation) {
      throw new Error("Service Location not found");
    }

    //2 - Validate program exists
    const program = await this.programRepo.getById(
      newServiceLocationProgram.programId
    );
    if (!program) {
      throw new Error("Program not found");
    }

    //3 - Validate validFrom and validTo dates are valid
    if (
      newServiceLocationProgram.validFrom >= newServiceLocationProgram.validTo
    ) {
      throw new Error("Invalid date range");
    }
  };

  public static async create(
    newServiceLocationProgram: ServiceLocationProgram
  ) {
    await this.validateServiceLocation(newServiceLocationProgram);

    newServiceLocationProgram.status = ServiceLocationProgramStatus.active;

    const serviceLocationProgram = await this.mainRepo.insert(
      newServiceLocationProgram
    );

    const visits: ServiceLocationProgramVisit[] = [];

    for (let i = 0; i < serviceLocationProgram.amountOfVisits; i++) {
      const visit = new ServiceLocationProgramVisit({
        serviceLocationProgramId: serviceLocationProgram._id,
        estimatedDate: new Date(),
        estimatedDuration: 0,
        status: ServiceLocationProgramVisitStatus.not_planned,
        visitNumber: i + 1,
      });
      visits.push(visit);
    }

    await this.serviceLocationProgramVisitRepo.insertMany(visits);

    return serviceLocationProgram;
  }

  public static async update(serviceLocationProgram: ServiceLocationProgram) {
    await this.validateServiceLocation(serviceLocationProgram);
    await this.mainRepo.update(serviceLocationProgram);
  }

  public static async delete(serviceLocationProgramId: string) {
    const serviceLocationProgram = await this.mainRepo.getById(
      serviceLocationProgramId
    );
    if (!serviceLocationProgram) {
      throw new Error("ServiceLocationProgram not found");
    }
    await this.mainRepo.delete(serviceLocationProgram);
  }

  public static async getById(serviceLocationProgramId: string) {
    return await this.mainRepo.getById(serviceLocationProgramId);
  }

  public static async getVisits(status: string, from: Date, to: Date) {
    return await this.serviceLocationProgramVisitRepo.getVisits(status, from, to);
  }

  public static async getPrograms(status: string, from: Date, to: Date){
    return await this.mainRepo.getPrograms(status, from, to);

  }
}
