import { Customer } from "entities/customer";
import { CustomerRepository } from "../repositories/customerRepository";

export class CustomerManager {
  static customerRepo = new CustomerRepository();

  public static create(customer: Customer) {
    return this.customerRepo.insert(customer);
  }

  public static edit(customer: Customer) {
    return this.customerRepo.update(customer);
  }

  public static async delete(customerId: string) {
    const customer = await this.customerRepo.getById(customerId);
    if (!customer) throw new Error("Customer does not exist");
    await this.customerRepo.delete(customer);
  }

  public static getById(customerId: string) {
    return this.customerRepo.getById(customerId);
  }

  public static getList(): Promise<Customer[]> {
    return this.customerRepo.getAll();
  }
}
