import { env } from "process";

const roles = {
    superAdmin: {id: 'superAdmin', label: 'Super Admin'},
    companyAdmin: {id: 'companyAdmin', label: 'Administrador'},
    employee: {id: 'employee', label: 'Colaborador'}
};

var config = {
    server:{
        port: process.env.PORT || 8080,
    },
    appSession:{
        hours: process.env.APP_SESSION_HOURS,
        secretKey: process.env.APP_JWT_KEY || "SJ_SYTEMS_2021",
        passwordSaltRounds: 10
    },
    database: {
        mongoUrl: process.env.MONGO_STRING,
        name: process.env.MONGO_DATABASE,
    },
    userAdmin:{
        email: process.env.USER_ADMIN_EMAIL,
        password: process.env.USER_ADMIN_PASSWORD,
    },
    publicConfig:{
        timezone: process.env.TIMEZONE || -3,
        companyName: process.env.COMPANY_NAME
    },
    roles,  
}

export default config;
