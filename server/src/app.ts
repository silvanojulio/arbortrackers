import * as dotenv from "dotenv";
dotenv.config();

import express, { Application, NextFunction, Response, Request } from "express";
import SessionRouter from "./routers/sessionRouter";
import UserRouter from "./routers/userRouter";
import LawnCareProgramEstimateRouter from "./routers/lawnCareProgramEstimateRouter";
import CustomerRouter from "./routers/customerRouter";
import ServiceLocationRouter from "./routers/serviceLocationRouter";
import ServiceLocationProgramRouter from "./routers/serviceLocationProgramRouter";
import cors from "cors";
import helmet from "helmet";
import path from "path";
import config from "./config";
import SecurityMiddleware from "./middlewares/securityMiddleware";
import errorHandler from "./middlewares/appErrorMiddleware";
import HtmlRouter from "./routers/htmlRouter";
import { getPdfUrlForEstimate } from "./utils/pdfProdivider";
import { sendTest } from "./utils/email";

const contextService = require("request-context");

const app: Application = express();

//--React--
app.use(express.static(path.join(__dirname, "public")));
app.get("/", (req: Request, res: Response, next: NextFunction) => {
  //console.log('>>> Sending index.html')
  res.sendFile(path.join(__dirname, "public", "index.html"));
});

app.use(helmet());
app.use(cors());
app.use(express.json());

app.use(contextService.middleware("CURRENT_REQUEST"));
app.use((req: Request, res: Response, next: NextFunction) => {
  contextService.set("CURRENT_REQUEST:request", req);
  next();
});

//--React--
app.use(express.static(path.join(__dirname, "public")));
app.get("/", (req: Request, res: Response, next: NextFunction) => {
  res.sendFile(path.join(__dirname, "public", "index.html"));
});

//--Public Endpoints
app.use("/api/session", SessionRouter);

app.get("/img/:file", (req, res) => {
  res.sendFile(path.join(__dirname, "assets", req.params.file));
});

app.get("/api/pdf-redirect/estimate/:id", (req, res) => {
  res.status(301).redirect(getPdfUrlForEstimate(req.params.id));
});

app.get("/api/test-email", async (req, res) => {
  await sendTest();
  res.send("email sent");
});

app.use("/api/html", HtmlRouter);

//-- Middleware
app.use(SecurityMiddleware);

//--Private Endpoints
app.use("/api/current-session", SessionRouter);
app.use("/api/users", UserRouter);

app.use("/api/lawn-care-program/estimate", LawnCareProgramEstimateRouter);
app.use("/api/customer", CustomerRouter);
app.use("/api/service-location", ServiceLocationRouter);
app.use("/api/service-location-program", ServiceLocationProgramRouter);

//ErrorHendler Endpoint
app.use(errorHandler);

console.clear();
app.listen(config.server.port, () =>
  console.log("Server running on " + config.server.port)
);
