import { IMongoEntityBase, MongoEntityBase } from "../db/mongoEntityBase";

export interface IDailyRoute extends IMongoEntityBase {
  date: Date;
  numericDate: number;
  technicianId: string;
  status: string;
  planComments?: string;
  technicianComments?: string;
  closedTime?: Date;
  estimatedTravel?: number;
}

export class DailyRoute extends MongoEntityBase implements IDailyRoute {
  constructor(param: IDailyRoute) {
    super();
    this._id = param._id;
    this.date = param.date;
    this.numericDate = param.numericDate;
    this.technicianId = param.technicianId;
    this.status = param.status;
    this.planComments = param.planComments;
    this.technicianComments = param.technicianComments;
    this.closedTime = param.closedTime;
    this.estimatedTravel = param.estimatedTravel;
  }
  date!: Date;
  numericDate!: number;
  technicianId!: string;
  status!: string;
  planComments?: string;
  technicianComments?: string;
  closedTime?: Date | undefined;
  estimatedTravel?: number | undefined;
}
