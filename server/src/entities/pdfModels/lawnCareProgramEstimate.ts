export interface EstimateItemPdfModel {
    title?: string;
    subtitle?: string;
    description?: string;
    explanation?: string;
    totalItem: string;
    
    showApplications?: boolean;
    applications?: number;

    showExtraOptions?: boolean;
    extraOptions?: string[];
}

export interface LawnCareEstimatePdfModel {

    header: {
        customerName: string;
        date: string;
        customerAddress: string;
        customerPhone?: string;
        customerEmail?: string;
        programType: string;
    };

    baseProgramItems: EstimateItemPdfModel[];
    supplementalServicesItems: EstimateItemPdfModel[];

    total: string;

    footer: {
        user: string;
        phone: string;
        email: string;
    }

}
