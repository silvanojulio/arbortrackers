import {
  MongoEntityBase,
  IMongoEntityBase,
  IMongoEntityBaseIdField,
} from "../db/mongoEntityBase";

export interface IServiceLocation extends IMongoEntityBaseIdField {
  address: string;
  state: string;
  city: string;
  customer_id: string;
  phone?: string;
  email?: string;
  contact?: string;
  point?: number[];
  isMain?: boolean;
  cp?: string;
  lastVisit?: Date;
}

export class ServiceLocation
  extends MongoEntityBase
  implements IServiceLocation
{
  constructor(param: IServiceLocation) {
    super();
    this._id = param._id;
    this.address = param.address;
    this.state = param.state;
    this.city = param.city;
    this.customer_id = param.customer_id;
    this.phone = param.phone;
    this.email = param.email;
    this.contact = param.contact;
    this.point = param.point;
    this.isMain = param.isMain;
    this.cp = param.cp;
    this.lastVisit = param.lastVisit;
  }
  address: string = "";
  state: string = "";
  city: string = "";
  customer_id: string = "";
  phone?: string;
  email?: string;
  contact?: string;
  point?: number[];
  isMain?: boolean;
  cp?: string;
  lastVisit?: Date;
}
