import { ICustomer } from "./customer";
import { IMongoEntityBase, MongoEntityBase } from "../db/mongoEntityBase";
import { IServiceLocation } from "./serviceLocation";
import { ObjectId } from "mongodb";

export interface IBaseProgramEstimateItem {
  code: string;
  cost?: number;
  applications?: number;
  extraOptions?: string[];
}

export class LawnCareProgramEstimate extends MongoEntityBase {
  constructor(
    public date: Date,
    public status: string,
    public serviceLocation_id: string,
    public customer_id: string,
    public baseProgramItems: IBaseProgramEstimateItem[],
    public supplementalServiceItems: IBaseProgramEstimateItem[],
    public userCreateId?: ObjectId
  ) {
    super();
  }
}

export interface ILawnCareProgramEstimateView extends IMongoEntityBase {
  date: Date;
  status: string;
  serviceLocation_id: string;
  customer_id: string;
  baseProgramItems: IBaseProgramEstimateItem[];
  supplementalServiceItems: IBaseProgramEstimateItem[];
  userCreateId?: string;
  customer: ICustomer;
  serviceLocation: IServiceLocation;
}
