import { IMongoEntityBase, MongoEntityBase } from "../db/mongoEntityBase";

export interface IServiceLocationInDailyRoute extends IMongoEntityBase {
  dailyRouteId: string;
  serviceLocationId: string;
  serviceLocationProgramId?: string;
  serviceLocationProgramVisitId?: string;
  order: number;
  status: string;
  planComments?: string;
  estimatedVisitTime?: Date;
  estimateDuration?: number;
  actualVisitTime?: Date;
  actualDuration?: number;
  technicianComments?: string;
}

export class ServiceLocationInDailyRoute
  extends MongoEntityBase
  implements IServiceLocationInDailyRoute
{
  constructor(param: IServiceLocationInDailyRoute) {
    super();
    this._id = param._id;
    this.dailyRouteId = param.dailyRouteId;
    this.serviceLocationId = param.serviceLocationId;
    this.order = param.order;
    this.status = param.status;
    this.planComments = param.planComments;
    this.estimatedVisitTime = param.estimatedVisitTime;
    this.estimateDuration = param.estimateDuration;
    this.actualVisitTime = param.actualVisitTime;
    this.actualDuration = param.actualDuration;
    this.technicianComments = param.technicianComments;
    this.serviceLocationProgramId = param.serviceLocationProgramId;
    this.serviceLocationProgramVisitId = param.serviceLocationProgramVisitId;
  }
  dailyRouteId!: string;
  serviceLocationId!: string;
  order!: number;
  status!: string;
  planComments?: string;
  estimatedVisitTime?: Date;
  estimateDuration?: number;
  actualVisitTime?: Date;
  actualDuration?: number;
  technicianComments?: string;
  serviceLocationProgramId?: string;
  serviceLocationProgramVisitId?: string;
}
