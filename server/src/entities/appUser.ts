import { MongoEntityBase } from "../db/mongoEntityBase";
import { EnumEntityBase } from "./appEnumEntityBase";

export class AppUser extends MongoEntityBase {
  constructor(
    public fullName: string,
    public email: string,
    public passwordHash: string,
    public active: Boolean,
    public role: Roles | null,
    public responsabilities?: string[]
  ) {
    super();
  }
}

export class Roles extends EnumEntityBase {
  public static SuperAdmin = new Roles(1, "Super Admin");
  public static CompanyAdmin = new Roles(2, "Company Admin");
  public static Employee = new Roles(3, "Employee");

  public static getAll = (): Array<Roles> =>
    new Array(Roles.SuperAdmin, Roles.CompanyAdmin, Roles.Employee);

  public static getById = (_id: number): Roles | null =>
    Roles.getAll().find((x) => x.id === _id) ?? null;

  public isEqual = (rol: Roles | null): boolean =>
    rol !== null && rol.id === this.id;
}
