export enum ServiceLocationProgramStatus {
  active = "active",
  completed = "completed",
  cancelled = "cancelled",
}

export enum ServiceLocationProgramVisitStatus {
  planned = "planned",
  not_planned = "not_planned",
  completed = "completed",
  cancelled = "cancelled",
}
