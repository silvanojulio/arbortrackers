export interface IProgramItem{
    code: string;
    title: string;
    subtitle?: string;
    description?: string;
    explanation?: string;
    applications?: boolean;
    extraOptions?: string[];
    showCost?: boolean;
}

export interface ILawnCareProgramTemplate{
    title: string;
    logoUrl?: string;
    descripcion: string;
    bottomText: string;
    author: {
        name: string;
        phone: string;
        email: string; 
    },
    baseProgram: {
        title: string;
        explanation: string;
        items: IProgramItem[]
    },
    suplementalProgram: {
        items: IProgramItem[]
    }
}
