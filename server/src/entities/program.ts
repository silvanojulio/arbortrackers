import { IMongoEntityBase, MongoEntityBase } from "../db/mongoEntityBase";

export interface IProgram extends IMongoEntityBase {
  name: string;
  description?: string;
  active: Boolean;
  price: number;
}

export class Program extends MongoEntityBase implements IProgram {
  constructor(param: IProgram) {
    super();
    this._id = param._id;
    this.name = param.name;
    this.description = param.description;
    this.active = param.active;
    this.price = param.price;
  }
  name!: string;
  description?: string;
  active!: Boolean;
  price!: number;
}
