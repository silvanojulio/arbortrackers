import { ICustomer } from "entities/customer";
import { IProgram } from "entities/program";
import { IServiceLocation } from "entities/serviceLocation";
import { IServiceLocationProgram } from "entities/serviceLocationProgram";
import { IServiceLocationProgramVisit } from "entities/serviceLocationProgramVisit";

export interface ServiceLocationProgramVisitView
  extends IServiceLocationProgramVisit {
  customer: ICustomer;
  serviceLocationProgram: IServiceLocationProgram;
  program: IProgram;
  serviceLocation: IServiceLocation;
}
