import { ICustomer } from "entities/customer";
import { IProgram } from "entities/program";
import { IServiceLocation } from "entities/serviceLocation";
import { IServiceLocationProgram } from "entities/serviceLocationProgram";

export interface ServiceLocationProgramView
  extends IServiceLocationProgram {
  customer: ICustomer;
  program: IProgram;
  serviceLocation: IServiceLocation;
}
