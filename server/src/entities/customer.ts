import { MongoEntityBase } from "../db/mongoEntityBase";

export class Customer extends MongoEntityBase {
  constructor(
    public fullName: string,
    public active: Boolean,
    public email?: string,
    public phone?: string,
    public externalId?: string
  ) {
    super();
  }
}

export interface ICustomer {
  _id: string;
  fullName: string;
  active: Boolean;
  email?: string;
  phone?: string;
  externalId?: string;
}
