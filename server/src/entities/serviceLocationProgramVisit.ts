import { ObjectId } from "mongodb";
import {
  IMongoEntityBase,
  IMongoEntityBaseIdField,
  MongoEntityBase,
} from "../db/mongoEntityBase";

export interface IServiceLocationProgramVisit extends IMongoEntityBaseIdField {
  serviceLocationProgramId: ObjectId;
  estimatedDate: Date;
  plannedDate?: Date;
  estimatedDuration: number;
  status: string;
  comments?: string;
  price?: number;
  visitNumber: number;
}

export class ServiceLocationProgramVisit
  extends MongoEntityBase
  implements IServiceLocationProgramVisit
{
  constructor(param: IServiceLocationProgramVisit) {
    super();
    this._id = param._id;
    this.serviceLocationProgramId = param.serviceLocationProgramId;
    this.estimatedDate = param.estimatedDate;
    this.plannedDate = param.plannedDate;
    this.estimatedDuration = param.estimatedDuration;
    this.status = param.status;
    this.comments = param.comments;
    this.price = param.price;
    this.visitNumber = param.visitNumber;
  }
  serviceLocationProgramId!: ObjectId;
  estimatedDate!: Date;
  plannedDate?: Date | undefined;
  estimatedDuration!: number;
  status!: string;
  comments?: string | undefined;
  price?: number | undefined;
  visitNumber!: number;
}
