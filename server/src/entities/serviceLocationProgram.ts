import { IMongoEntityBase, MongoEntityBase } from "../db/mongoEntityBase";

export interface IServiceLocationProgram extends IMongoEntityBase {
  serviceLocationId: string;
  programId: string;
  status: string;
  programComments: string;
  validFrom: Date;
  validTo: Date;
  estimatedPrice: number;
  amountOfVisits: number;
  details?: {
    description: string;
    value: string;
  }[];
}

export class ServiceLocationProgram
  extends MongoEntityBase
  implements IServiceLocationProgram
{
  constructor(param: IServiceLocationProgram) {
    super();
    this._id = param._id;
    this.serviceLocationId = param.serviceLocationId;
    this.status = param.status;
    this.programComments = param.programComments;
    this.validFrom = param.validFrom;
    this.validTo = param.validTo;
    this.estimatedPrice = param.estimatedPrice;
    this.amountOfVisits = param.amountOfVisits;
    this.details = param.details;
    this.programId = param.programId;
  }
  serviceLocationId!: string;
  programId!: string;
  status!: string;
  programComments!: string;
  validFrom!: Date;
  validTo!: Date;
  estimatedPrice: number;
  amountOfVisits!: number;
  details?: { description: string; value: string }[];
}
